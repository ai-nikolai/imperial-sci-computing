/************************************************************************
Nikolai Rozanov,
Feb 2016,
00831231,
M3SC
************************************************************************/

#include <stdio.h>
#include <math.h>

#define max(x,y) (((x)>(y)) ? (x) : (y))
#define ABS_(x) (  ((x)>=0) ? (x) : (-(x))  )

/************************************************************************
Helper Functions
************************************************************************/

double pow_2_(long int exp){
    double divisor;
    *((long int *) &divisor) =  exp<<52;
    return divisor;
}


// for solving linear equations
int lin_root(double a1, double a0, double * r){
/*<Rozanov>,<Nikolai>,<M3SC>*/
    //special cases
    if(a1==0){
        if(a0!=0){
            return -1;
        }
        return 0;
    }
    //standard cases
    *r = -a0/a1;
    return 1;
}


int quad_root(double a2, double a1, double a0, double * r1, double * r2, int * case_, double * num){
    /*<ROZANOV>,<NIKOLAI>,<M3SC>*/
    /*****************************************************
    *variable declaration
    *****************************************************/
    int i;
    double det, pow_2=1.0;
    long int my_exponent, a2_mask, a1_mask, a0_mask, min_mask, max_mask;



    /****************************************************
    *the linear case:
    *****************************************************/

    if (a2==0){
        i = lin_root(a1,a0,r1);
        return i-2;
    }

    // a0 = 0 case:
    if(a0==0){
        i = lin_root(a2,a1,r1); // as a2 is != 0, we have that the linear equation always has one root

        //returning the value and roots the right way!
        if(*r1<0){
            *r2 = 0;
            return 2;
        }
        if (*r1==0){
            *r2 = 0;
            return 1;
        }
        if (*r1>0){
            *r2 = *r1;
            *r1 = 0;
            return 2;
        }
    }


    /****************************************************
    *the quadratic case:
    *****************************************************/
    if (a1==0){
        if(-a0/a2<0){
            *r1 = 0;
            *r2 = sqrt(a0/a2);
            return 0;
        }
        else{
            *r1 = -sqrt(-a0/a2);
            *r2 = -(*r1);
            return 2;
        }
    }


    /***********************
    *Diskriminant
    ************************/

    //determining the exponent(and sign) of the coefficients

    a2_mask = ((*((long int *) &a2)) >> 52);//  //determines the exponent of a double
    a1_mask = ((*((long int *) &a1)) >> 52);//
    a0_mask = ((*((long int *) &a0)) >> 52);//

    /*
    *MASKS to make use of a2_mask, a1_mask, a0_mask
    *
    *04000 to determine sign bit
    *03777 get rid of the sign bit.
    *
    */

    //determining min/max of the exponent of the coefficients
    min_mask    = fmin((a2_mask & 03777),fmin(a1_mask & 03777,a0_mask & 03777)); //minimum exponent of a2,a1,a0
    max_mask    = fmax((a2_mask & 03777),fmax(a1_mask & 03777,a0_mask & 03777)); //maximum exponent of a2,a1,a0


    /*************
    *taking care of various computational cases to compute Diskriminant
    **************/
    if( max_mask>(1023+12) ){

        (*case_) = 1;

        my_exponent = 2046-max_mask; // determining by how much to divide
        *((long int *) &pow_2) =  my_exponent<<52; //creating the divisor (such that double precision is preserved)


    }
    else if( min_mask<(1023-24)){

        (*case_) = 2;
        //my_exponent = 622+(1023-min_mask); // determining by how much to mulitply
        //my_exponent = 1023-((1023-24)-min_mask); //determining by how much to divide
        my_exponent = 2046-min_mask;
        *((long int *) &pow_2) =  my_exponent<<52;//creating the divisor (such that double precision is preserved)

    }
    else{ //standard case

        (*case_) = 3;

    }



    //
    a2 = a2*pow_2;
    a1 = a1*pow_2;
    a0 = a0*pow_2;
    det = (a1*a1)-((double)4)*(a2*a0) ;

    if(det<0){ //checking for the complex case
        //printf("Case5\n");
        *r1 = -a1/(((double)2) * a2);
        *r2 = sqrt(-det)/(((double)2) * ABS_(a2));
        return 0;
    }

    det = sqrt(det);




    //testing
    *num = det;


    /****************************
    *solving the actual equation
    *****************************/

        //checking for det = 0 case
    if (det==0){

        (*case_) = 4;

        //printf("Case4\n");
        *r1 = -a1/(((double)2) * a2);
        *r2 = *r1;
        return 1;
    }


    //two distinct real roots (floating point version)
    if (a1<0){

        (*case_) = 5;
        //printf("Case3\n");
        *r2 = (-a1 + det) /  (((double)2) * a2) ;
    }
    else{
        (*case_) = 6;
        //printf("Case2\n");
        *r2 = -(a1 + det) /  (((double)2) * a2);
    }


    //getting the other root.

    if (ABS_((*r2))>pow_2_(1023-10)){
        *r1 = (a0/a2)/(*r2);
    }

    else{
        *r1 = -(a1 + det) /  (((double)2) * a2);
    }



    //ordering roots
    if ((*r1)>(*r2)){
        det = (*r2);
        (*r2) = (*r1);
        (*r1) = det;
    }

    return 2;


}



/************************************************************************
Main Function
************************************************************************/
int main(void){
    int case_;
    double a2,a1,a0, r1,r2, det;
    /*********************************************************
    //Section for my Information
    *********************************************************/
    // declaring the strings and i
    int my_int_for_info;
    char fields[5][14] = {"Name","CID","LIBRARY NO","Email Adress","Course CODE"};
    char values[5][33] = {"Rozanov, Nikolai","00831231","0247023908","nikolai.rozanov13@imperial.ac.uk", "M3SC"};

    //printing in automated way
    for(my_int_for_info = 0; my_int_for_info<5; my_int_for_info++){
        printf("%12s: <%s>\n",fields[my_int_for_info], values[my_int_for_info]);
    }
    printf("\n\n");
    /*********************************************************
    //Main Part of the Program
    *********************************************************/
    // getting the Coefficients
    printf("Enter the coefficients of the Quadratic Equation a2*x^2+a1*x+a0=0 \n");
    printf("In the order a2,a1,a0, separated by spaces\n");
    scanf("%lf %lf %lf",&a2,&a1,&a0);
    // printing the roots given the type of equation.
    switch(quad_root(a2,a1,a0,&r1,&r2, &case_, &det)){
        case 2:
            printf("Two distinct real roots: x1 = %lf, x2 = %lf\n",r1, r2 );
            break;
        case 1:
            printf("One real root with double multiplicity: x = %lf \n", r1);
            break;
        case 0:
            printf("Two Complex roots \n");
            break;
        case -1:
            printf("One real root, with single multiplicity (linear equation): x = %lf\n", r1);
            break;
        case -2:
            printf("All values for x satisfy the equation!\n");
            break;
        case -3:
            printf("The equation has no solution!\n");
            break;
        default:
            printf("Error in the code, this case does not exist! \n");
    }
    return 0;
}




//
