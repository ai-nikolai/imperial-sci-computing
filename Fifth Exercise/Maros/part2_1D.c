#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

void mat_init(double ***,int, int);
void vec_init(double ** ,int);

void get_A_mat(double ***,int);
void get_sparse_A_mat(double ***, int);
void get_rhs(double **, int);

void print_mat(double **, int,int);
void print_vec(double *, int);

void vec_copy(double *, double *,int);
double vec_max(double *, int);

double *Gauss(double **, double *, int);
double *BGauss(double **,double *, int, int);

clock_t timer(clock_t,int);
double get_time(clock_t);

void free_mat(double **);
void free_vec(double *);


int main(){

    int N;
    int i;
    long int pow=1;

    int start_i=3, max_N=25;

    clock_t time;
    double time_;

    double ** A;
    double * psi_inside;
    //double psi_boundary[2] = {0,0};
    double * rhs;

    double max_;


    /*
    for(i=start_i;i<=max_N;i++){
        N = (double) (pow<<i);

        get_A_mat(&A,N);
        get_rhs(&rhs,N);
        //print_vec(rhs,N-1);
        //print_mat(A,N-1,N-1);

        psi_inside = Gauss(A,rhs,N-1);

        print_vec(psi_inside,N-1);

        free_vec(rhs);
        free_vec(psi_inside);
        free_mat(A);
    }
    */

    for(i=start_i;i<=max_N;i++){
        N = (double) (pow<<i);

        get_sparse_A_mat(&A,N);
        get_rhs(&rhs,N);
        //print_vec(rhs,N-1);
        //print_mat(A,N-1,N-1);
        time = timer(time,0);
        psi_inside = BGauss(A,rhs,N-1,1);
        time = timer(time,1);
        time_ = get_time(time);

        max_ = vec_max(psi_inside,N-1);
        //print_vec(psi_inside,N-1);
        printf("Max:%.15lf Time: %.15lf N: %d\n",max_, time_,i);

        free_vec(rhs);
        free(psi_inside);
        free_mat(A);
    }



    return 0;
}
