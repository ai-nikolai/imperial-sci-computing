#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#ifdef _OPENMP
#include <omp.h>
#endif

/* Janco, Maros, 00866730, M3SC*/


double *BGauss(double ** C, double * y, int N, int B)
{

	int i, j, k=2, n;
	double sum, *x;

	#ifdef _OPENMP
	 omp_set_num_threads(3);
	 int limit = 512;
	#endif

	x = (double *) calloc(N , sizeof(double))-1;
	if (x == NULL) { fprintf(stderr, "Error: Out of memory!\n"); exit(EXIT_FAILURE); }

	/* Gauss elimination */

	if (B > 0)
	{
		for (i = 1; i <= N-B-1  ; ++i, k = i+1)
		{
			for (j = B; j >= 1; --j, k++)
			{
				if (C[k][j] != 0.)
				{
					C[k][j] /= C[i][B+1];

					#pragma omp parallel for reduction(-:C[k][n]) if (B > limit) //firstprivate(i)
					for (n = j+1; n <=B+j; ++n)
					{
						C[k][n] -= C[k][j] * C[i][k-i+n];
					}

					y[k] -= C[k][j] * y[i];
				}

			}
		}

		for (; i <= N-1  ; ++i, k = i+1)
		{
			for (j = B; k <= N; --j, k++)
			{
				if (C[k][j] != 0.)
				{
					C[k][j] /= C[i][B+1];

					#pragma omp parallel for reduction(-:C[k][n]) if (N-i > limit)
					for (n = j+1; n <=N-i+j; ++n)
					{
						C[k][n] -= C[k][j] * C[i][k-i+n];
					}

					y[k] -= C[k][j] * y[i];
				}

			}
		}

		/* Backward substitution */

		k = B + 1;

		x[N] = y[N]/C[N][k];

		if (B == 1)
		{
			for (i = N-1; i>=1; --i)
				{
					x[i] = (y[i]- C[i][k+1]*x[i+1])/C[i][k];
				}
		}

		else
		{

			for (i=N-1; i>=N-1+B; --i)
				{
					sum = C[i][k+1]*x[i+1];
					#pragma omp parallel for reduction(+:sum) if (N-i > limit)
					for (j = 2; j<=N-i; ++j)
					{
						sum += C[i][k+j]*x[i+j];
					}

					x[i] = (y[i]- sum)/C[i][k];
				}

			for (; i>=1; --i)
				{
					sum = C[i][k+1]*x[i+1];
					#pragma omp parallel for reduction(+:sum) if (B > limit)
					for (j = 2; j<=B; ++j)
					{
						sum += C[i][k+j]*x[i+j];
					}

					x[i] = (y[i]- sum)/C[i][k];
				}
		}
	}

	else if (B == 0)
	{
		#pragma omp parallel for if (N>limit)
		for (i = 1; i<=N; i++)
		{
			x[i] = y[i]/C[i][1];
		}
	}

	else
	{
		fprintf(stderr, "Error: Inconsistent parameter B!\n");
	    exit(EXIT_FAILURE);
	}

	return x;
}
