#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "mat.h"

/*matrix_lib*/
void mat_init(double ***,int, int);
void vec_init(double ** ,int);
void print_mat(double **, int,int);
void print_vec(double *, int);
max_t vec_max(double *, int);
void free_mat(double **);
void free_vec(double *);
xy_max get_xy(max_t,int);


/*Part2_1D_lib*/
void get_A_mat_2D(double ***,int);
void get_sparse_A_mat_2D(double ***, int);
void get_rhs_rough_2D(double **, int);
void get_rhs_smooth_2D(double **, int);
double Gauss_counter(double );
double BGauss_counter(double,double);
clock_t timer(clock_t,int);
double get_time(clock_t);
void make_csv(const char* , double *, int);

/*Gauss BGauss*/
double *Gauss(double **, double *, int);
double *BGauss(double **,double *, int, int);


int main(){

    int N;
    int i,j;
    long int num;
    int temp;

    int start_i=3, max_N=2, max_B=3;

    clock_t time;
    double time_N_r,time_B_r,time_N_s,time_B_s;
    double dx;

    double ** A;
    double * psi_inside;
    double * rhs;

    max_t max_N_r, max_B_r,max_N_s, max_B_s;
    xy_max xy_N_r, xy_N_s, xy_B_r, xy_B_s;

    /*********************************************************
    //Section for my Information
    *********************************************************/
    // declaring the strings and i
    int my_int_for_info;
    char fields[5][14] = {"Name","CID","LIBRARY NO","Email Adress","Course CODE"};
    char values[5][33] = {"Rozanov, Nikolai","00831231","0247023908","nikolai.rozanov13@imperial.ac.uk", "M3SC"};

    //printing in automated way
    for(my_int_for_info = 0; my_int_for_info<5; my_int_for_info++){
        printf("%12s: <%s>\n",fields[my_int_for_info], values[my_int_for_info]);
    }
    printf("%12s: <%s>\n","Date:", __DATE__);
    printf("%12s: <%s>\n","Time:", __TIME__);

    printf("\n\n");
    /*********************************************************
    //Main Part of the Program
    *********************************************************/

    char names[9][19]={"N","Max Psi Rough","max x rough","max y rough","Max Psi Smooth", "max x smooth","max x smooth","Gauss Time","BGauss Time"};
    char string[2][19]= {"Gauss out of Mem","BGauss out of Mem"};

    printf("%4s\t%17s\t%17s\t%17s\t%17s\t%17s\t%17s\t%19s\t%19s\n",names[0],names[1],names[2],names[3],names[4],names[5],names[6],names[7],names[8]);
    for(i=start_i;i<=max_B;i++){

        N = 500;
        time_N_s = 0;
        dx = 1.0/((double)N);
        temp = (N-1)*(N-1);
        //BGauss

        get_sparse_A_mat_2D(&A,N);
        //print_mat(A,temp,2*(N-1)+1);
        //rough
        get_rhs_rough_2D(&rhs,N);
        //print_vec(rhs, temp);

        time = timer(time,0);
        psi_inside = BGauss(A,rhs,temp,N-1);
        time = timer(time,1);
        //multiply_row(psi_inside,dx,N-1);

        //print_vec(psi_inside, temp);

        time_B_r = get_time(time);
        max_B_r = vec_max(psi_inside,temp);
        xy_B_r = get_xy(max_B_r,N);

        free_vec(rhs);
        free_vec(psi_inside);
        free_mat(A);


        //smooth

        get_sparse_A_mat_2D(&A,N);
        get_rhs_smooth_2D(&rhs,N);
        //print_mat(A,temp,2*N+1);

        time = timer(time,0);
        psi_inside = BGauss(A,rhs,temp,N-1);
        time = timer(time,1);
        //multiply_row(psi_inside,dx,N-1);

        time_B_s = get_time(time);
        max_B_s = vec_max(psi_inside,temp);
        xy_B_s = get_xy(max_B_s,N);

        free_vec(rhs);
        if(i>max_B){
            make_csv("2D.csv",psi_inside,temp);
        }
        free_vec(psi_inside);
        free_mat(A);


        //Gauss
        if(i<=max_N){
            get_A_mat_2D(&A,N);
            //print_mat(A,temp,temp);

            //print_vec(rhs, temp);

            //rough
            get_rhs_rough_2D(&rhs,N);

            time = timer(time,0);
            psi_inside = Gauss(A,rhs,temp);
            time = timer(time,1);


            //multiply_row(psi_inside,dx,N-1);

            time_N_r = get_time(time);
            max_N_r = vec_max(psi_inside,temp);
            xy_N_r = get_xy(max_B_r,N);

            free_vec(rhs);
            free_vec(psi_inside);
            free_mat(A);
            //smooth

            get_A_mat_2D(&A,N);
            get_rhs_smooth_2D(&rhs,N);

            time = timer(time,0);
            psi_inside = Gauss(A,rhs,(N-1)*(N-1));
            time = timer(time,1);

            //multiply_row(psi_inside,dx,N-1);
            time_N_s = get_time(time);
            max_N_s = vec_max(psi_inside,(N-1)*(N-1));
            xy_N_s = get_xy(max_B_r,N);

            free_vec(rhs);
            free_vec(psi_inside);



            free_mat(A);
            printf("2^%2d\t%17.15lf\t%17.15lf\t%17.15lf\t%17.15lf\t%17.15lf\t%17.15lf\t%19.15lf\t%19.15lf\n",i,max_N_r.val,xy_N_r.x*dx,xy_N_r.y*dx ,max_N_s.val,xy_N_s.x*dx,xy_N_s.y*dx,time_N_r,time_B_s);
        }
        else{
            printf("2^%2d\t%17.15lf\t%17.15lf\t%17.15lf\t%17.15lf\t%17.15lf\t%17.15lf\t%19s\t%19.15lf\n",i,max_B_r.val,xy_B_r.x*dx,xy_B_r.y*dx ,max_B_s.val,xy_B_r.x*dx,xy_B_r.y*dx,string[0],time_B_s);

        }



    }
    printf("2^%2d\t%17.15lf\t%17.15lf\t%17.15lf\t%17.15lf\t%17.15lf\t%17.15lf\t%19s\t%19s\n",i,NAN,NAN,NAN,NAN,NAN,NAN,string[0],string[1]);





    return 0;
}
