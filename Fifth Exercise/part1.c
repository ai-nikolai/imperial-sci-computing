#include <stdio.h>
#include <stdlib.h>

void mat_init(double ***,int, int);
void vec_init(double ** ,int);

void get_mat(double **, int, int);
void get_B_mat(double ***, double **, int, int);
void get_vec(double *, int);

void vec_copy(double *, double *,int);

void print_mat(double **, int,int);
void print_vec(double *, int);

double * Gauss(double **, double *, int);
double *BGauss(double **,double *, int, int);

void free_mat(double **);
void free_vec(double *);


int main(){

    int N,M;

    int B = 2;
    double ** mat;
    double ** bmat;
    double * x;
    double * y;
    double * z;

    /*********************************************************
    //Section for my Information
    *********************************************************/
    // declaring the strings and i
    int my_int_for_info;
    char fields[5][14] = {"Name","CID","LIBRARY NO","Email Adress","Course CODE"};
    char values[5][33] = {"Rozanov, Nikolai","00831231","0247023908","nikolai.rozanov13@imperial.ac.uk", "M3SC"};

    //printing in automated way
    for(my_int_for_info = 0; my_int_for_info<5; my_int_for_info++){
        printf("%12s: <%s>\n",fields[my_int_for_info], values[my_int_for_info]);
    }
    printf("%12s: <%s>\n","Date:", __DATE__);
    printf("%12s: <%s>\n","Time:", __TIME__);

    printf("\n\n");
    /*********************************************************
    //Main Part of the Program
    *********************************************************/

    scanf("%d\n",&N);
    M=N;
    vec_init(&y,N);
    vec_init(&z,N);
    mat_init(&mat,N,M);
    get_mat(mat,N,M);
    get_vec(y,N);
    vec_copy(y,z,N);
    get_B_mat(&bmat,mat,N,B);
    //print_mat(bmat,N,2*B+1);

    x = Gauss(mat,y,N);
    //print_mat(mat,N,M);
    printf("The solution vector is:\n");
    print_vec(x,N);
    free_vec(x);

    printf("\n");

    x = BGauss(bmat,z,N,B);
    //print_mat(bmat,N,2*B+1);
    print_vec(x,N);
    //print_mat(mat,N,M);
    //print_mat(bmat,N,2*B+1);

    free_vec(y);
    free_vec(z);
    free_vec(x);
    free_mat(bmat);
    free_mat(mat);


    return 0;
}
