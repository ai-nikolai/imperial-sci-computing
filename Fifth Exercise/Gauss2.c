#include <stdio.h>
#include <stdlib.h>


void mat_init(double ***,int, int);
void vec_init(double ** ,int);

void subtract_rows(double *, double *, double, int, int);


void gauss_helper(double ** A, double * y, int N){
    int i,j;
    double temp;
    for(i=1;i<=N-1;i++){
        for(j=i+1;j<=N;j++){
            if(A[j][i]!=0){
                temp = A[j][i]/A[i][i];
                A[j][i]=0;
                subtract_rows(A[i],A[j],-temp,i+1,N);
                y[j] = y[j] - temp*y[i];
            }

        }
    }
}

void backward_helper(double ** A, double * y, double * x, int N){
    int i,j;
    for(i=N;i>=1;i--){
        x[i]=y[i]/A[i][i];
        for(j=1;j<=i-1;j++){
            y[j]=y[j]-x[i]*A[j][i];
        }
    }
}

double * Gauss(double ** A, double * y, int N){
    double * x;
    vec_init(&x,N);

    gauss_helper(A,y,N);
    backward_helper(A,y,x,N);

    return x;
}
