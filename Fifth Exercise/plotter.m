
  %...domain size
  xLen  = 1;
  yLen  = 1;


  p     = 8;
  q     = 8;

  N     = 2^p-1;
  M     = 2^q-1;

  x     = linspace(0,xLen,N+1);
  y     = linspace(0,yLen,M+1);
  xc    = (x(1:end-1)+x(2:end))/2;
  yc    = (y(1:end-1)+y(2:end))/2;
  [xx,yy] = meshgrid(xc,yc);


u = dlmread('2D.csv');


u2D  = reshape(u,N,N);


 figure(1)
  %colormap(mmap)
  surf(xx,yy,u2D);
  shading interp;
  lighting phong;
  camlight headlight;
  material shiny;
  view(45,35)
  axis off

  figure(2)
  %colormap(mmap)
  contourf(xx,yy,u2D,30,'EdgeColor','None')
  axis image

  figure(3)
  semilogy(rr,'-o'); grid on
