#include <stdio.h>
#include <stdlib.h>
#include "mat.h"
//#include <omp.h>


void free_mat(double ** mat){
    free(mat[1]+1);
    free(mat+1);
}

void free_vec(double * vec){
    free(vec+1);
}

void row_printer(double * row, int M){
    int i;
    for(i=1;i<=M;i++){
        printf("%3.5lf ",row[i]);
    }
    printf("\n");
}

void print_mat(double ** mat, int N,int M){
    int i;
    for(i=1;i<=N;i++){
        row_printer(mat[i],M);
    }
}

void print_vec(double * vec, int N){
    int i;
    for(i=1;i<=N;i++){
        printf("%.5lf\n",vec[i]);
    }
}

//called by mat_helper only
void mat_helper(double *** mat, int N, int M){
    int i,j;
    (*mat)[1] = (double *) calloc(N*M,sizeof(double))-1;
    if((*mat)[1]==NULL){
        fprintf(stderr,"ERROR2 mat init\n");
        exit(EXIT_FAILURE);
    }
    for(i=2;i<=N;i++){
        (*mat)[i] = (*mat)[i-1]+M;
    }
}


void mat_init(double *** mat,int N, int M){

    (*mat) = (double **) calloc(N,sizeof(double *))-1;
    if((*mat)==NULL){
        fprintf(stderr,"ERROR mat init\n");
        exit(EXIT_FAILURE);
    }
    mat_helper(mat,N,M);
}

void vec_init(double ** vec,int N){

    (*vec) = (double *) calloc(N,sizeof(double))-1;
    if((*vec)==NULL){
        fprintf(stderr,"ERROR vec init\n");
        exit(EXIT_FAILURE);
    }
}



void make_ascending(double ** mat, int N, int M){
    int i,j;

    for(i=1;i<=N;i++){
        for(j=1;j<=M;j++){
            mat[i][j] = (double) ((i-1)*N + j);
        }
    }
}

void get_mat(double ** mat, int N, int M){
    int i,j;

    for(i=1;i<=N;i++){
        for(j=1;j<=M;j++){
            scanf("%lf ",&mat[i][j]);
        }
        scanf("\n");
    }
}

void get_vec(double * vec, int N){
    int i;
    for(i=1;i<=N;i++){
        scanf("%lf\n",&vec[i]);
    }
}


void multiply_row(double * row, double a, int N){
    int i;
    for(i=1;i<=N;i++){
        row[i] *= a;
    }
}

void vec_copy(double * y, double * z,int N){
    int i;
    for(i=1;i<=N;i++){
        z[i] = y[i];
    }
}

max_t vec_max(double * vec, int N){
    int i;
    max_t max_ = {1,vec[1]};
    for(i=2;i<=N;i++){
        if(vec[i]>max_.val){
            max_.val=vec[i];
            max_.index=i;
        }
    }
    return max_;
}
