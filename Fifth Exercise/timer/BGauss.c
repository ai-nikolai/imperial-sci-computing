#include <stdio.h>
#include <stdlib.h>

void mat_init(double ***,int, int);
void vec_init(double ** ,int);

/*
*Helper functions
*/
void subtract_rows_G(double * row1, double * row2, double a,int start,int N){
    //#pragma omp parallel for
    for(;start<=N;start++){
        row2[start] += a*row1[start];
    }
}


/*
*Main Functions
*/
void get_B_mat(double *** B_mat, double ** A, int N, int B){
    int i,j;
    mat_init(B_mat,N,2*B+1);
    for(i=-B;i<=B;i++){
        for(j=1;j<=N;j++){
            if(i+j<1 || i+j>N){
                (*B_mat)[j][i+B+1] = 0;
            }
            else{
                (*B_mat)[j][i+B+1] = A[j][i+j];
            }
        }
    }
}



inline void B_helper(double ** C, double * y, int N,int B){
    //int counter=0;
    int i,j,k;
    double temp;
    for(i=1;i<=N-B;i++){
        for(j=1;j<=B;j++){
            k = i+j;
            temp = C[k][B+1-j] / C[i][B+1];
            subtract_rows_G(&C[i][j],C[k],-temp,B+2-j,2*B+1-j);
            y[k] -= temp*y[i];
            //counter += 2*(B)+3;
        }
    }
    for(i=1;i<=B-1;i++){
        for(j=1;j<=B-i;j++){
            k=i+N-B;
            //if(C[k+j][B+1-j]!=0){
            temp = C[k][B+1-j] / C[i][B+1];
            subtract_rows_G(&C[k][j],C[k+j],-temp,B+2-j,2*B+1-j-i);
            y[k+j] -= temp*y[k];
            //counter += 2*(B-i)+3;
            //}
        }
    }
    //printf("Here:%d\n", counter);
}

inline void B_backward_helper(double ** C, double * y, double * x, int N,int B){
    int i,j;
    //int counter=0;
    for(i=N;i>=B+1;i--){
        x[i] = y[i]/C[i][B+1];
        //counter+=1;
        for(j=1;j<=B;j++){
            y[i-j] -= x[i]*C[i-j][B+1+j];
            //counter+=2;
        }

    }
    for(i=B;i>=1;i--){
        x[i] = y[i]/C[i][B+1];
        //counter+=1;
        for(j=1;j<=i-1;j++){
            y[i-j] -= x[i]*C[i-j][B+1+j];
            //counter+=2;
        }
    }
    //printf("Here:%d\n", counter);
}

double *BGauss(double ** C,double * y, int N, int B){
    double * x;
    vec_init(&x,N);

    B_helper(C,y,N,B);
    B_backward_helper(C,y,x,N,B);

    return x;
}
