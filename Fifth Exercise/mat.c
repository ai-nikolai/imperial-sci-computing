    /*<ROZANOV>,<NIKOLAI>,<M3SC>*/
#include <stdio.h>
#include <stdlib.h>
#include "mat.h"
//#include <omp.h>


void free_mat(double ** mat){
    /*<ROZANOV>,<NIKOLAI>,<M3SC>*/
    free(mat[1]+1);
    free(mat+1);
}

void free_vec(double * vec){
    /*<ROZANOV>,<NIKOLAI>,<M3SC>*/
    free(vec+1);
}

void row_printer(double * row, int M){
    /*<ROZANOV>,<NIKOLAI>,<M3SC>*/
    int i;
    for(i=1;i<=M;i++){
        printf("%3.5lf ",row[i]);
    }
    printf("\n");
}

void print_mat(double ** mat, int N,int M){
    /*<ROZANOV>,<NIKOLAI>,<M3SC>*/
    int i;
    for(i=1;i<=N;i++){
        row_printer(mat[i],M);
    }
}

void print_vec(double * vec, int N){
    /*<ROZANOV>,<NIKOLAI>,<M3SC>*/
    int i;
    for(i=1;i<=N;i++){
        printf("%.5lf\n",vec[i]);
    }
}

//called by mat_init only
void mat_helper(double *** mat, int N, int M){
    /*<ROZANOV>,<NIKOLAI>,<M3SC>*/
    int i,j;
    (*mat)[1] = (double *) calloc(N*M,sizeof(double))-1;
    if((*mat)[1]==NULL){
        fprintf(stderr,"ERROR2 mat init\n");
        exit(EXIT_FAILURE);
    }
    for(i=2;i<=N;i++){
        (*mat)[i] = (*mat)[i-1]+M;
    }
}


void mat_init(double *** mat,int N, int M){
    /*<ROZANOV>,<NIKOLAI>,<M3SC>*/

    (*mat) = (double **) calloc(N,sizeof(double *))-1;
    if((*mat)==NULL){
        fprintf(stderr,"ERROR mat init\n");
        exit(EXIT_FAILURE);
    }
    mat_helper(mat,N,M);
}

void vec_init(double ** vec,int N){
    /*<ROZANOV>,<NIKOLAI>,<M3SC>*/

    (*vec) = (double *) calloc(N,sizeof(double))-1;
    if((*vec)==NULL){
        fprintf(stderr,"ERROR vec init\n");
        exit(EXIT_FAILURE);
    }
}


void get_mat(double ** mat, int N, int M){
    /*<ROZANOV>,<NIKOLAI>,<M3SC>*/
    int i,j;

    for(i=1;i<=N;i++){
        for(j=1;j<=M;j++){
            scanf("%lf ",&mat[i][j]);
        }
        scanf("\n");
    }
}

void get_vec(double * vec, int N){
    /*<ROZANOV>,<NIKOLAI>,<M3SC>*/
    int i;
    for(i=1;i<=N;i++){
        scanf("%lf\n",&vec[i]);
    }
}

max_t vec_max(double * vec, int N){
    /*<ROZANOV>,<NIKOLAI>,<M3SC>*/
    int i;
    max_t max_ = {1,vec[1]};
    for(i=2;i<=N;i++){
        if(vec[i]>max_.val){
            max_.val=vec[i];
            max_.index=i;
        }
    }
    return max_;
}

xy_max get_xy(max_t max_, int N){
    /*<ROZANOV>,<NIKOLAI>,<M3SC>*/
    xy_max xy;
    xy.x=max_.index/(N-1)+1;
    xy.y=max_.index%(N-1);
    return xy;
}
