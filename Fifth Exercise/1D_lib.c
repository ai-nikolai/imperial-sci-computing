#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "mat.h"



void mat_init(double ***,int, int);
void vec_init(double ** ,int);

void print_vec(double *, int);


/*************
*1D
*************/
double Gauss_counter(double N){
    return (N*N/3.0*N*2.0 + N/2.0*N -7.0/6.0*N);
}

double BGauss_counter_1D(double N){
    return(8*N-10);
}

void get_rhs_rough_1D(double ** rhs, int N){
    int i;
    double dx = 1.0/((double)N);
    vec_init(rhs,N-1);

    for(i=1;i<=N-1;i++){
        if(i>N/4 && i<=N/2){
            (*rhs)[i]=-80*dx*dx;
        }
        else{
            (*rhs)[i]=0;
        }
    }

}
void get_rhs_smooth_1D(double ** rhs, int N){
    int i;
    double dx = 1.0/((double)N);
    vec_init(rhs,N-1);

    for(i=1;i<=N-1;i++){
        if(i==N/4 || i==N/2){
            (*rhs)[i]=-40*dx*dx;
        }
        else if(i>N/4 && i<N/2){
            (*rhs)[i]=-80*dx*dx;
        }
        else{
            (*rhs)[i]=0;
        }
    }

}

void get_A_mat_1D(double *** A,int N){
    int i;
    mat_init(A,N-1,N-1);

    for(i=1;i<=N-2;i++){
        (*A)[i][i]=-2;
        (*A)[i+1][i]=1;
        (*A)[i][i+1]=1;
    }
    (*A)[N-1][N-1]=-2;
}

void get_sparse_A_mat_1D(double *** A, int N){
    int i;
    mat_init(A,N-1,3);

    for(i=1;i<=N-2;i++){
        (*A)[i+1][1]=1;
        (*A)[i][2]=-2;
        (*A)[i][3]=1;
    }
    (*A)[N-1][2]=-2;

}


clock_t timer(clock_t start, int case_){
    if(case_==0){
        return clock();
    }
    if(case_==1){
        return (clock()-start);
    }
    else return 0;
}

double get_time(clock_t time){
    return ((double) time )/((double) CLOCKS_PER_SEC);
}
