#include <stdio.h>
#include <stdlib.h>


void mat_init(double *** ,int , int);

void vec_init(double ** ,int);

void subtract_rows(double *, double *, double, int, int);

/*
*Main Functions
*/
void get_B_mat(double *** B_mat, double ** A, int N, int B){
    int i,j;
    mat_init(B_mat,N,2*B+1);
    for(i=-B;i<=B;i++){
        for(j=1;j<=N;j++){
            if(i+j<1 || i+j>N){
                (*B_mat)[j][i+B+1] = 0;
            }
            else{
                (*B_mat)[j][i+B+1] = A[j][i+j];
            }
        }
    }
}
/*
for(j=1;j<=B;j++){
    k = i+j;
    temp = C[k][B+1-j] / C[i][B+1];
    subtract_rows_G(&C[i][j],C[k],-temp,B+2-j,2*B+1-j);
    y[k] -= temp*y[i];
    //counter += 2*
*/
void B_helper(double ** C, double * y, int N,int B){
    //int counter=0;
    int i,j,k,l;
    double temp;
    for(i=1;i<=N-B;i++){
        for(j=1;j<=B;j++){
            k = i+j;
            temp = C[i+j][B+1-j]/C[i][B+1];
            y[k] -= temp*y[i];
            for(l=B+2-j;l<=2*B+1-j;l++){
                C[i+j][l] -= temp*(&C[i][j])[l];
            }
        }
    }
    for(i=1;i<=B-1;i++){
        for(j=1;j<=B-i;j++){
            k=i+N-B;
            //if(C[k+j][B+1-j]!=0){
            temp = C[k+j][B+1-j]/C[k][B+1];

            for(l=B+2-j;l<=2*B+1-j-i;l++){
                C[k+j][l] -= temp*(&C[k][j])[l];
            }

            y[k+j] -= temp*y[k];
            //counter += 2*(B-i)+3;
            //}
        }
    }
    //printf("Here:%d\n", counter);
}

void B_backward_helper(double ** C, double * y, double * x, int N,int B){
    int i,j;
    //int counter=0;
    for(i=N;i>=B+1;i--){
        x[i] = y[i]/C[i][B+1];
        //counter+=1;
        for(j=1;j<=B;j++){
            y[i-j] -= x[i]*C[i-j][B+1+j];
            //counter+=2;
        }

    }
    for(i=B;i>=1;i--){
        x[i] = y[i]/C[i][B+1];
        //counter+=1;
        for(j=1;j<=i-1;j++){
            y[i-j] -= x[i]*C[i-j][B+1+j];
            //counter+=2;
        }
    }
    //printf("Here:%d\n", counter);
}

double *BGauss(double ** C,double * y, int N, int B){
    double * x;
    vec_init(&x,N);

    B_helper(C,y,N,B);
    B_backward_helper(C,y,x,N,B);

    return x;
}
