#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "mat.h"



void mat_init(double ***,int, int);
void vec_init(double ** ,int);

void print_vec(double *, int);


/*************
*1D
*************/
double Gauss_counter(double N){
    return (N*N/3.0*N*2.0 + N/2.0*N -7.0/6.0*N);
}
/***********
* 2D
***********/

void get_rhs_rough_2D(double ** rhs, int N){
    int i,j;
    double dx = 1.0/((double)N);
    vec_init(rhs,(N-1)*(N-1));

    for(i=N/4;i<=N/2;i++){
        for(j=N/4;j<=N/2;j++){

            (*rhs)[(N-1)*(i-1)+j]=-100*dx*dx;

        }
    }
}

void get_rhs_smooth_2D(double ** rhs, int N){
    int i,j;
    double dx = 1.0/((double)N);
    vec_init(rhs,(N-1)*(N-1));

    for(i=1;i<=N-1;i++){
        for(j=1;j<=N-1;j++){

            if(i>=N/4 && i<=N/2 && j>=N/4 && j<=N/2){

                (*rhs)[(N-1)*(i-1)+j]=-100*dx*dx;
            }

        }
    }
}

void get_A_mat_2D(double *** A,int N){
    int i=N-1;
    mat_init(A,i*i,i*i);
    //setting the main diagonal
    for(i=1;i<=(N-1)*(N-1)-1;i++){
        (*A)[i][i]=-4;
        if((i%(N-1))!=0){ //taking care of 2D skipping
             (*A)[i+1][i]=1; //lower diagonal
             (*A)[i][i+1]=1; //upper diagonal
        }
    }
    (*A)[(N-1)*(N-1)][(N-1)*(N-1)]=-4;
    for(i=N;i<=(N-1)*(N-1);i++){
        (*A)[i][i-N+1]=1;
        (*A)[i-N+1][i]=1;
    }
}

void get_sparse_A_mat_2D(double *** A, int N){
    int i=N-1;
    mat_init(A,i*i,2*i+1);
    //main diagonals
    for(i=1;i<=(N-1)*(N-1)-1;i++){
        (*A)[i][N]=-4;
        if((i%(N-1))!=0){ //taking care of 2D skipping
             (*A)[i+1][N-1]=1; //lower diagonal
             (*A)[i][N+1]=1; //upper diagonal
         }
    }
    (*A)[(N-1)*(N-1)][N]=-4;
    //other diagonals
    for(i=N;i<=(N-1)*(N-1);i++){
        (*A)[i][1]=1;
        (*A)[(N-1)*(N-1)-i+1][2*(N-1)+1]=1;
    }

}



clock_t timer(clock_t start, int case_){
    if(case_==0){
        return clock();
    }
    if(case_==1){
        return (clock()-start);
    }
    else return 0;
}

double get_time(clock_t time){
    return ((double) time )/((double) CLOCKS_PER_SEC);
}


void make_csv(const char* file_name, double * vec, int N){
	int i;
	FILE * file = fopen(file_name,"w");
	for (i=1;i<=N;i++){
		fprintf(file, "%.15lf\n", vec[i]);
	}
	fprintf(file, "%.15lf", vec[i]);
	fclose(file);
}
