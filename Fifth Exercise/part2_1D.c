#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "mat.h"


/*matrix_lib*/
void mat_init(double ***,int, int);
void vec_init(double ** ,int);
void print_mat(double **, int,int);
void print_vec(double *, int);
void vec_copy(double *, double *,int);
max_t vec_max(double *, int);
void free_mat(double **);
void free_vec(double *);


/*Part2_1D_lib*/
void get_A_mat_1D(double ***,int);
void get_sparse_A_mat_1D(double ***, int);
void get_rhs_rough_1D(double **, int);
void get_rhs_smooth_1D(double **, int);
double Gauss_counter(double );
double BGauss_counter_1D(double );
clock_t timer(clock_t,int);
double get_time(clock_t);

/*Gauss BGauss*/
double *Gauss(double **, double *, int);
double *BGauss(double **,double *, int, int);






int main(){

    int N;
    int i,j;
    long int pow=1;

    int start_i=3, max_N=13, max_B=24;

    clock_t time;
    double time_N_r,time_B_r,time_N_s,time_B_s;
    double dx;

    double ** A;
    double * psi_inside;
    double * rhs;

    double count_N, count_B;

    max_t max_N_r, max_B_r,max_N_s, max_B_s;


    /*********************************************************
    //Section for my Information
    *********************************************************/
    // declaring the strings and i
    int my_int_for_info;
    char fields[5][14] = {"Name","CID","LIBRARY NO","Email Adress","Course CODE"};
    char values[5][33] = {"Rozanov, Nikolai","00831231","0247023908","nikolai.rozanov13@imperial.ac.uk", "M3SC"};

    //printing in automated way
    for(my_int_for_info = 0; my_int_for_info<5; my_int_for_info++){
        printf("%12s: <%s>\n",fields[my_int_for_info], values[my_int_for_info]);
    }
    printf("%12s: <%s>\n","Date:", __DATE__);
    printf("%12s: <%s>\n","Time:", __TIME__);

    printf("\n\n");
    /*********************************************************
    //Main Part of the Program
    *********************************************************/

    char names[9][19]={"N","Max Psi Rough","max x rough","Max Psi Smooth", "max x smooth","Gauss Time","Gauss Flops","BGauss Time","BGauss Flops"};
    char string[2][19]= {"Gauss out of Mem","BGauss out of Mem"};

    printf("%4s\t%17s\t%17s\t%17s\t%17s\t%19s\t%19s\t%19s\t%19s\n",names[0],names[1],names[2],names[3],names[4],names[5],names[6],names[7],names[8]);
    for(i=start_i;i<=max_B;i++){

        N = (pow<<i);
        time_N_s = 0;
        dx = 1.0/((double)N);

        //BGauss

        get_sparse_A_mat_1D(&A,N);

        //rough
        get_rhs_rough_1D(&rhs,N);

        time = timer(time,0);
        psi_inside = BGauss(A,rhs,N-1,1);
        time = timer(time,1);
        //multiply_row(psi_inside,dx,N-1);

        time_B_r = get_time(time);
        max_B_r = vec_max(psi_inside,N-1);
        count_B = BGauss_counter_1D((double) N)/1.0e9/time_B_r;

        free_vec(rhs);
        free_vec(psi_inside);
        free_mat(A);

        //smooth

        get_sparse_A_mat_1D(&A,N);
        get_rhs_smooth_1D(&rhs,N);

        time = timer(time,0);
        psi_inside = BGauss(A,rhs,N-1,1);
        time = timer(time,1);
        //multiply_row(psi_inside,dx,N-1);

        time_B_s = get_time(time);
        max_B_s = vec_max(psi_inside,N-1);

        free_vec(rhs);
        free_vec(psi_inside);
        free_mat(A);


        //Gauss
        if(i<=max_N){

            get_A_mat_1D(&A,N);


            //rough
            get_rhs_rough_1D(&rhs,N);

            time = timer(time,0);
            psi_inside = Gauss(A,rhs,N-1);
            time = timer(time,1);
            //multiply_row(psi_inside,dx,N-1);

            time_N_r = get_time(time);
            max_N_r = vec_max(psi_inside,N-1);
            count_N = Gauss_counter((double) N)/1.0e9/time_N_r;

            free_vec(rhs);
            free_vec(psi_inside);
            free_mat(A);
            //smooth

            get_A_mat_1D(&A,N);
            get_rhs_smooth_1D(&rhs,N);

            time = timer(time,0);
            psi_inside = Gauss(A,rhs,N-1);
            time = timer(time,1);

            //multiply_row(psi_inside,dx,N-1);
            time_N_s = get_time(time);
            max_N_s = vec_max(psi_inside,N-1);

            free_vec(rhs);
            free_vec(psi_inside);



            free_mat(A);
            printf("2^%2d\t%17.15lf\t%17.15lf\t%17.15lf\t%17.15lf\t%19.15lf\t%19.15lf\t%19.15lf\t%19.15lf\n",i,max_N_r.val,max_N_r.index*dx, max_N_s.val,max_N_s.index*dx,time_N_r,count_N,time_B_s,count_B);
        }
        else{
            printf("2^%2d\t%17.15lf\t%17.15lf\t%17.15lf\t%17.15lf\t%19s\t%19s\t%19.15lf\t%19.15lf\n",i,max_B_r.val,max_B_r.index*dx, max_B_s.val,max_B_s.index*dx,string[0],string[0],time_B_s,count_B);

        }



    }
    printf("2^%2d\t%17.15lf\t%17.15lf\t%17.15lf\t%17.15lf\t%19s\t%19s\t%19s\t%19s\n",i,NAN,NAN,NAN,NAN,string[0],string[1],string[0],string[1]);





    return 0;
}
