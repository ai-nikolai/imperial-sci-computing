#include <math.h>
#include <stdio.h>
#include <float.h>
#include <stdlib.h>
#include <time.h>

#define pi M_PI




int FastTN(double *, double*, double*, double*, int, int);

/******************
* HELPER
******************/

clock_t timer(clock_t start, int case_){
    if(case_==0){
        return clock();
    }
    if(case_==1){
        return (clock()-start);
    }
    else return 0;
}

double get_time(clock_t time){
    return ((double) time )/((double) CLOCKS_PER_SEC);
}

void print_time(double time){
    printf("Time:%lf\n", time);
}






void printer(double * vec, int N){
    int i;
    for(i=1;i<N;i++){
        printf("%10.6lf ",vec[i]);
    }
    printf("\n");
}


void printer2(double * vec, int N,int skip){
    int i;
    for(i=1;i<N;i++){
        printf("%10.6lf ",vec[i*skip]);
    }
    printf("\n");
}


double * zeros(int N){
    double * vec = (double *) calloc(N,sizeof(double));
    return vec;
}

double * ones(int N){
    int i;
    double * vec = (double *) malloc(sizeof(double)*N);
    for(i=0;i<N;i++){
        vec[i]=1;
    }
    return vec;
}

double * asc(int N){
    int i;
    double * vec = (double *) malloc(sizeof(double)*N);
    for(i=0;i<N;i++){
        vec[i]=i;
    }
    return vec;
}





double get_error(double * x, double * y, int N){
    int i;
    double result=0;

    for(i=1;i<N;i++){
        result += (x[i]-y[i])*(x[i]-y[i]);
    }

    return sqrt(result);
}

/**************
* Q1
**************/

int get_p(int N){
    int power=0, N_cpy=N;
    while (N_cpy%2==0  &&  N_cpy>2){
        //printf("%d %d ",pow,N_cpy);
        N_cpy = N >> (++power);
        //printf("%d %d\n",pow,N_cpy);
    }
    return N_cpy;
}

int get_pow(int N){
    int power=0, N_cpy=N;
    while (N_cpy%2==0  &&  N_cpy>2){
        //printf("%d %d ",pow,N_cpy);
        N_cpy = N >> (++power);
        //printf("%d %d\n",pow,N_cpy);
    }
    return power;
}


double* SFactors(int N){
    int i,j,k,tracker=1,p,power;
    double * vec = (double *) malloc((N/2)*sizeof(double));
    if(vec==NULL){
        exit(1);
    }
    p = get_p(N);
    power = get_pow(N);

    vec[0]=1;


    if(p==3){
        vec[tracker] = sin(pi/3);
        tracker++;
    }
    if(p==5){
        vec[tracker] = sin(pi/5);
        tracker++;
        vec[tracker] = sin(2*pi/5);
        tracker++;
    }

    for(i=1;i<=power;i++){
        k=(1<<i)*p;
        for(j=1;(2*j-1)<k/2;j++){
            vec[tracker]=sin((2*j-1)*pi/k);
            tracker ++;
        }
    }

    return vec;
}



/*******************
* direct matrix multiplication
********************/

int mat_mult(double *x, double * y, int N){
    int i,j;
    for(i=1;i<=N-1;i++){
        for(j=1;j<=N-1;j++){
            x[i] += y[j]*sin(i*j*pi/N);
        }
    }
    return 0;
}



/***************
* Fast Transforms
***************/

int FastUN(double *x, double *y, double *w, double *S, int N, int skip){
    int k;
    double temp;

    if(N==2){
        x[skip]     = y[skip]*S[2] + y[2*skip]*S[3];
        x[2*skip]   = y[skip]*S[3] - y[2*skip]*S[2];
        return 0;
    }

    if(N==3){
        temp = y[2*skip]*S[4];
        x[skip]     = y[skip]*S[3] + temp + y[3*skip]*S[5];
        x[2*skip]   = (y[skip] - y[3*skip])*S[4] + temp;
        x[3*skip]   = y[skip]*S[5] - temp + y[3*skip]*S[3];
        return 0;
    }

    if(N==5){
        temp = S[7]*y[3*skip];
        x[skip]=    S[5]*y[skip] + S[6]*y[2*skip] + temp + S[8]*y[4*skip] + S[9]*y[5*skip];
        x[2*skip]=  S[6]*y[skip] + S[9]*y[2*skip] + temp - S[5]*y[4*skip] - S[8]*y[5*skip];
        x[3*skip]=  S[7]*y[skip] + S[7]*y[2*skip] - temp - S[7]*y[4*skip] + S[7]*y[5*skip];
        x[4*skip]=  S[8]*y[skip] - S[5]*y[2*skip] - temp + S[9]*y[4*skip] - S[6]*y[5*skip];
        x[5*skip]=  S[9]*y[skip] - S[8]*y[2*skip] + temp - S[6]*y[4*skip] + S[5]*y[5*skip];
        return 0;
    }


//    y: even u, odd v
    for(k=1;k<N/2;k++){
        w[2*k*skip]     = y[(N+1-2*k)*skip] -   y[(N-2*k)*skip]; // u
        w[(2*k-1)*skip] = y[(2*k)*skip]     +   y[(2*k+1)*skip]; // v
    }

    w[N*skip] = y[skip]; //u
    w[(N-1)*skip] = y[N*skip]; //v

    FastTN(y,w,x,S,N/2,skip*2); //u == a
    FastTN(y-skip,w-skip,x-skip,S,N/2,skip*2);//v == b

    for(k=1;k<=N/2;k+=2){
        x[k*skip] = y[(2*k)*skip] * S[N+k-1]  +   y[(2*k-1)*skip] * S[2*N-k] ;
        x[(N+1-k)*skip] = y[(2*k)*skip] * S[2*N-k] -  y[(2*k-1)*skip] * S[N+k-1];
    }
    for(k=2;k<=N/2;k+=2){
        x[k*skip] = - y[(2*k)*skip] * S[N+k-1]  +   y[(2*k-1)*skip] * S[2*N-k] ;
        x[(N+1-k)*skip] = - (y[(2*k)*skip] * S[2*N-k]  +  y[(2*k-1)*skip] * S[N+k-1]);
    }
    return 0;
}

int FastTN(double *x, double *y, double *w, double *S, int N, int skip){
    int k;
    double temp,temp2,temp3,temp4;

    if(N==2){
        temp = y[skip]*S[1];
        x[skip]     = temp + y[2*skip];
        x[2*skip]   = temp - y[2*skip];
        return 0;
    }
    if(N==3){
        temp = S[2]*y[skip]+y[3*skip];
        temp2 = S[1]*y[2*skip];
        x[skip]     = temp + temp2;
        x[2*skip]   = y[skip] - y[3*skip];
        x[3*skip]   = temp - temp2;
        return 0;
    }
    if(N==5){
        temp  = S[3]*y[skip]+S[4]*y[3*skip]+y[5*skip];
        temp2 = S[4]*y[skip]+S[3]*y[3*skip]-y[5*skip];
        temp3 = S[1]*y[2*skip]+S[2]*y[4*skip];
        temp4 = S[2]*y[2*skip]-S[1]*y[4*skip];
        x[skip]=    temp + temp3;
        x[2*skip]=  temp2 + temp4;
        x[3*skip]=  y[skip]-y[3*skip]+y[5*skip];
        x[4*skip]=  temp2 - temp4;
        x[5*skip]=  temp - temp3;
        return 0;
    }

    FastTN(w,y,x,S,N/2,skip*2);
    FastUN(w-skip,y-skip,x-skip,S,N/2,skip*2);

    for(k=1;k<=N/2;k++){
        x[k*skip] = w[(2*k-1)*skip]  +  w[(2*k*skip)];
        x[(N+1-k)*skip] = w[(2*k-1)*skip]  -  w[(2*k*skip)];
    }



    return 0;
}

int FastSN(double *x, double *y, double *w, double *S, int N, int skip){
    int k;
    double temp,temp2,temp3,temp4;


    if(N==3){
        x[skip]=(y[skip]+y[2*skip])*S[1];
        x[2*skip]=(y[skip]-y[2*skip])*S[1];;
        return 0;
    }
    if(N==5){
        temp  =y[skip]+y[4*skip];
        temp2 =y[skip]-y[4*skip];
        temp3 =y[2*skip]-y[3*skip];
        temp4 =y[2*skip]+y[3*skip];
        x[skip]=    S[1]*temp+S[2]*temp4;
        x[2*skip]=  S[2]*temp2+S[1]*temp3;
        x[3*skip]=  S[2]*temp-S[1]*temp4;
        x[4*skip]=  S[1]*temp2-S[2]*temp3;
        return 0;
    }
    if(N==4){
        temp = (y[3*skip]+y[skip])*S[1];
        x[skip]= y[2*skip]+temp;
        x[2*skip]=y[skip]-y[3*skip];
        x[3*skip]= -y[2*skip]+temp;
        return 0;
    }
    if(N==2){
        x[skip]=y[skip];
        return 0;
    }
    //    printer2(y,N,skip);
    //partitioning y into s and a vecs for recursive formula.
    for(k=1;k<N/2;k++){
        w[2*k*skip] = y[k*skip] - y[(N-k)*skip];
        w[(2*k-1)*skip] = y[k*skip] + y[(N-k)*skip];
    }
    w[(N-1)*skip] = y[N/2*skip];
    //printer2(w,N,skip);
    FastSN(x,w,y,S,N/2,2*skip);
    FastTN(x-skip,w-skip,y-skip,S,N/2,2*skip);

    return 0;
}



/***************
* Main function
***************/

int main(int argc, const char ** argv){
    int power=atoi(argv[1]), prime=atoi(argv[2]);
    int i, N = prime*(1<<power);

    clock_t clock;
    double time;

    double * x = zeros(N+1);
    double * w = zeros(N+1);
    double * y = ones(N+1);
    double * s = SFactors(N);

    //double * y = asc(N+1);
    //double * z = asc(N+1);

    printf("N:2^%dx%d\n",power,prime);
    //printer(x,N);
    clock = timer(clock,0);
    FastSN(x,y,w,s,N,1);
    FastSN(w,x,y,s,N,1);
    clock = timer(clock,1);
    time = get_time(clock);
    print_time(time);
    //printer(x,N);

    //printer(x2,N);
    clock = timer(clock,0);
    mat_mult(x2,z,N);
    clock = timer(clock,1);
    time = get_time(clock);
    print_time(time);
    //printer(x2,N);

    printf("ERROR:%lf\n",get_error(x,x2,N));
    printf("check %lf %lf %lf %lf %lf\n",s[5]-sin(5*pi/12),s[2]-sin(pi/6),x[1]-x2[1],x[2]-x2[2],x[3]-x2[3]);

    free(x);
    free(w);
    free(y);
    free(s);

    return 0;
}
