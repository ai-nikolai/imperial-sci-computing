/************************************************************************
Nikolai Rozanov,
Feb 2016,
00831231,
M3SC
************************************************************************/
#include <stdio.h>
#include <math.h>
#include <complex.h>

#define max(x,y) (((x)>(y)) ? (x) : (y))
#define ABS_(x) (  ((x)>=0) ? (x) : (-(x))  )

/************************************************************************
Helper Functions
************************************************************************/


void printer(double a, double b, const char * text){
    printf("%s %.16lf %lx  %.16lf %lx\n", text, a, *((long int *) &a),  b, *((long int *) &b)  );
}


void comparer(double test, double copy){

    long int mask, mask2, mask3, temp, temp2, temp3;

    mask = *((long int *) &test);
    mask2 = (mask>>52) & 03777;
    mask3 = (mask<<(64-52))>>(64-52);

    temp = *((long int *) &copy);
    temp2 = (temp>>52) & 03777;
    temp3 = (temp<<(64-52))>>(64-52);


    printf("\nHexadecimal Complete:%li %li\n",mask, temp);
    printf("Hexadecimal Complete:%li\n",mask-temp);
    printf("realvalue: %li %li\n", mask2, temp2);
    printf("Hexadecimal Complete:%li\n",mask2-temp2);
    printf("realvalue: %li %li\n", mask3, temp3);
    printf("Hexadecimal Complete:%li\n",mask3-temp3);
}


long int long_min(long int x, long int y){
    return ((x<=y) ? x : y);
}

long int long_max(long int x, long int y){
    return ((x>=y) ? x : y);
}

//to use give pow_2 a long int, it will create a power of 2, accoridng to IEEE standards, ie 2^i, i[-1023,1024]
double pow_2_(long int exp){
    double divisor;
    *((long int *) &divisor) =  exp<<52;
    return divisor;
}

/*
 *
 *
 * QUAD_ROOT (ASSESSMENT)
 *
 *
 */

 int lin_root(double a1, double a0, double * r){
     //special cases
     if(a1==0){
         if(a0!=0){
             return -1;
         }
         return 0;
     }

     //standard cases
     *r = -a0/a1;
     return 1;
 }




 int quad_root(double a2, double a1, double a0, double * r1, double * r2){
     /*<ROZANOV>,<NIKOLAI>,<M3SC>*/
     /*****************************************************
     *variable declaration
     *****************************************************/
     int i;
     double det, pow_2=1.0;
     long int my_exponent=0, a2_mask, a1_mask, a0_mask, min_mask, max_mask;

     /****************************************************
     *the linear case:
     *****************************************************/

     if (a2==0){
         i = lin_root(a1,a0,r1);
         return i-2;
     }

     // a0 = 0 case:
     if(a0==0){
         i = lin_root(a2,a1,r1); // as a2 is != 0, we have that the linear equation always has one root
         //returning the value and roots the right way!
         if(*r1<0){
             *r2 = 0;
             return 2;
         }
         if (*r1==0){
             *r2 = 0;
             return 1;
         }
         if (*r1>0){
             *r2 = *r1;
             *r1 = 0;
             return 2;
         }
     }
     /****************************************************
     *the quadratic case:
     *****************************************************/
     if (a1==0){
         if(-a0/a2<0){
             *r1 = 0;
             *r2 = sqrt(a0/a2);
             return 0;
         }
         else{
             *r1 = -sqrt(-a0/a2);
             *r2 = -(*r1);
             return 2;
         }
     }
     /***********************
     *Diskriminant
     ************************/

     //determining the exponent(and sign) of the coefficients

     a2_mask = ((*((long int *) &a2)) >> 52);//  //determines the exponent of a double
     a1_mask = ((*((long int *) &a1)) >> 52);//
     a0_mask = ((*((long int *) &a0)) >> 52);//

     /*
     *MASKS to make use of a2_mask, a1_mask, a0_mask
     *
     *04000 to determine sign bit
     *03777 get rid of the sign bit.
     *
     */

     //determining min/max of the exponent of the coefficients
     min_mask    = fmin((a2_mask & 03777),fmin(a1_mask & 03777,a0_mask & 03777)); //minimum exponent of a2,a1,a0
     max_mask    = fmax((a2_mask & 03777),fmax(a1_mask & 03777,a0_mask & 03777)); //maximum exponent of a2,a1,a0

     /*************
     *taking care of various computational cases to compute Diskriminant
     **************/
     if( (max_mask>(1023+24))     ||     (((max_mask-min_mask)*2)>=1023) ){
         my_exponent += 2046-max_mask; // determining by how much to divide
         pow_2 = pow_2_(my_exponent); //creating the divisor (such that double precision is preserved)
     }
     else if( min_mask<(1023-24) ){

         my_exponent += 2046-min_mask;
         pow_2 = pow_2_(my_exponent);//creating the divisor (such that double precision is preserved)
     }
     else{ //standard case
     }

     /*
     *
     *main part of quadratic equation
     */
     a2 = a2*pow_2;
     a1 = a1*pow_2;
     a0 = a0*pow_2;
     det = (a1*a1)-((double)4)*(a2*a0) ;

     if(det<0){ //checking for the complex case
         //printf("Case5\n");
         *r1 = -a1/(((double)2) * a2);
         *r2 = sqrt(-det)/(((double)2) * ABS_(a2));
         return 0;
     }

     det = sqrt(det);

     /****************************
     *solving the actual equation
     *****************************/

         //checking for det = 0 case
     if (det==0){
         *r1 = -a1/(((double)2) * a2);
         *r2 = *r1;
         return 1;
     }

     //two distinct real roots (floating point version)
     if (a1<0){
         *r2 = (-a1 + det) /  (((double)2) * a2) ;
     }
     else{
         *r2 = -(a1 + det) /  (((double)2) * a2);
     }

     //getting the other root.
     if (ABS_((*r2))>pow_2_(1023-10)){
         *r1 = (a0/a2)/(*r2);
     }

     else{
         *r1 = -(a1 + det) /  (((double)2) * a2);
     }

     //ordering roots
     if ((*r1)<(*r2)){
         det = (*r2);
         (*r2) = (*r1);
         (*r1) = det;
     }
     return 2;
 }


/************************************************************************
Main Function
************************************************************************/
int main(void){

    /*******************************************************************
    ********************************************************************
    SOURCE for Radius:
        Charon: https://en.wikipedia.org/wiki/Charon_(moon)
        Pluto: https://en.wikipedia.org/wiki/Pluto
    ********************************************************************
    *******************************************************************/

    int t=0,i=0,j=0,k, done=0x0, r3, counter1=0, counter2=0, counter3=0, counter4=0, counter5=0;    //doubles
    double a2_,a1_,a0_, r1_dbl, r2_dbl, r1, r2;

    int case_ = 0;
    double det=0, two = 0;
    double benchmark = 1e-2;
    /*********************************************************
    //Main Part of the Program
    *********************************************************/

    //printf("%16s\t%16s\t%16s\t%16s\t%16s\t%16s\n",array_fields[0],array_fields[1],array_fields[2],array_fields[3],array_fields[4],array_fields[5]);



    while (done==0){
        scanf("%lf %lf %lf %d %lf %lf %d", &a2_,&a1_,&a0_,&done,&r1,&r2,&r3);
        t++;
        k = quad_root(a2_,a1_,a0_,&r1_dbl,&r2_dbl, &case_, &det, &two);
        //k = quad_roots_giovanni(a2_,a1_,a0_,&r1_dbl,&r2_dbl);


        if (r3>=0){


            if (ABS_(r1_dbl-r1)>benchmark){
                //printf(" here : %lf  %lf\n",ABS_(r1_dbl-r1), r1_dbl-r1);
                counter1++;
                (case_) += 20;
            }

            if (ABS_(r2_dbl-r2)>benchmark){
                counter2++;
                (case_) += 200;

            }
            if (k!=r3){
                counter3++;
                (case_) += 3000;
            }


            i++;
        }

        if (r3==-1){

            if (ABS_(r1_dbl-r1)>benchmark){
                counter5++;
                (case_) +=40000;
            }

            if (k!=r3){
                counter4++;
                (case_) += 500000;
            }

            j++;
        }

        if ((case_) > 19){
            printf("\n\n\nCASE:%d   %d  %10.5e %10.5e %10.5e %3.16lf %3.16lf %d\n",t,(case_),a2_,a1_,a0_,r1,r2,r3);
            printf("ROOT Diffs:%3.16lf   %3.16lf return: %d his return: %d\n",r1_dbl-r1,r2_dbl-r2,k, r3);
            printf("ROOTS:%3.16lf   %3.16lf return: %d his return: %d\n",r1_dbl,r2_dbl,k, r3);
            printf("DET: %3.16lf\n", det);
            printf("A2:%10.5e A1:%3.16lf A0:%10.5e EXP:%10.5e\n",a2_*two,a1_*two,a0_*two,two);
        }

        (case_)=0;


    }
    printf("\nBenchmark value: %1.3e\n", benchmark);
    printf("Quadratic cases failed: r1: %d  r2: %d! Passing precentage: %f\n", counter1, counter2,((float)(2*i-counter1-counter2))/((float)(2*i)));
    printf("Quadtratic Return: %d out of: %d passed! Passing precentage: %f \n",i-counter3,i,((float)(i-counter3))/((float)i));

    //printf("Linear cases failed: %d ! Passing precentage: %f\n", counter5,((float)(j-counter5))/((float)(j)));
    //printf("Linear Return: %d out of: %d passed! Passing precentage: %f \n",j-counter4,j,((float)(j-counter4))/((float)j));


    i += j;
    counter3 += counter4;
    printf("TOTAL Return: %d out of: %d passed! Passing precentage: %f\n\n",i-counter3,i,((float)(i-counter3))/((float)i) );

    return 0;
}




//
