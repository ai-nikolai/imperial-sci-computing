/************************************************************************
Nikolai Rozanov,
Feb 2016,
00831231,
M3SC
************************************************************************/
#include <stdio.h>
#include <math.h>
#include <complex.h>


/************************************************************************
Helper Functions
************************************************************************/
/*
long int long_min(long int x, long int y){
    return ((x<=y) ? x : y);
}

long int long_max(long int x, long int y){
    return ((x>=y) ? x : y);
}

//to use give pow_2 a long int, it will create a power of 2, accoridng to IEEE standards, ie 2^i, i[-1023,1024]
double pow_2(long int exp){
    double divisor;
    *((long int *) &divisor) =  exp<<52;
    return divisor;
}
*/
/************************************************************************
Equation Solvers
************************************************************************/
// for solving linear equations
int lin_root(double a1, double a0, double * r){
    /*<ROZANOV>,<NIKOLAI>,<M3SC>*/
    //special cases
    if(a1==0){
        if(a0!=0){
            return -1;
        }
        return 0;
    }

    //standard cases
    *r = -a0/a1;
    return 1;
}

// for solving linear equations (float version)
int lin_root_float(float a1, float a0, float * r){
    /*<ROZANOV>,<NIKOLAI>,<M3SC>*/
    //special cases
    if(a1==0){
        if(a0!=0){
            return -1;
        }
        return 0;
    }
    //standard cases
    *r = -a0/a1;
    return 1;
}


//for solving quadratic equations
int quad_root_traditional(double a2, double a1, double a0, double * r1, double * r2){
    /*<ROZANOV>,<NIKOLAI>,<M3SC>*/

    //variable declaration
    int i;
    double det;

    // the linear case:
    if (a2==0){
        i = lin_root(a1,a0,r1);
        return i-2;
    }

    // a0 = 0 case:
    if(a0==0){
        i = lin_root(a2,a1,r1); // as a2 is != 0, we have that the linear equation always has one root

        //returning the value and roots the right way!
        if(*r1>0){
            *r2 = 0;
            return 2;
        }
        if (*r1==0){
            *r2 = 0;
            return 1;
        }
        if (*r1<0){
            *r2 = *r1;
            *r1 = 0;
            return 2;
        }
    }
    //quadratic case:
    det = a1*a1-4*a2*a0;
    //checking for the complex case
    if (det<0){
        return 0;
    }
        //checking for det = 0 case
    if (det==0){
        *r1 = -a1*(0.5/a2);
        *r2 = *r1;
        return 1;
    }
        //two distinct real roots
    *r1 = (-a1+sqrt(det))*(0.5/a2);
    *r2 = (-a1-sqrt(det))*(0.5/a2);
    return 2;
}

//for solving quadratic equations for assessment (precise)
int quad_root_float_traditional(float a2, float a1, float a0, float * r1, float * r2){
    /*<ROZANOV>,<NIKOLAI>,<M3SC>*/
    //variable declaration
    int i;
    float det;

    // the linear case:
    if (a2==0){
        i = lin_root_float(a1,a0,r1);
        return i-2;
    }

    // a0 = 0 case:
    if(a0==0){
        i = lin_root_float(a2,a1,r1); // as a2 is != 0, we have that the linear equation always has one root

        //returning the value and roots the right way!
        if(*r1>0){
            *r2 = 0;
            return 2;
        }
        if (*r1==0){
            *r2 = 0;
            return 1;
        }
        if (*r1<0){
            *r2 = *r1;
            *r1 = 0;
            return 2;
        }
    }

    //quadratic case:
    det = a1*a1-4*a2*a0;

        //checking for the complex case
    if (det<0){
        return 0;
    }
        //checking for det = 0 case
    if (det==0){
        *r1 = -a1*(0.5/a2);
        *r2 = *r1;
        return 1;
    }
    //two distinct real roots (floating point version)
    *r1 = (-a1+sqrt(det))*(0.5/a2);
    *r2 = (-a1-sqrt(det))*(0.5/a2);
    return 2;


}

//for solving quadratic equations for assessment (precise)
int quad_root_float(float a2, float a1, float a0, float * r1, float * r2){
    /*<ROZANOV>,<NIKOLAI>,<M3SC>*/
    //variable declaration
    int i;
    float det;
    // the linear case:
    if (a2==0){
        i = lin_root_float(a1,a0,r1);
        return i-2;
    }
    // a0 = 0 case:
    if(a0==0){
        i = lin_root_float(a2,a1,r1);
        if(*r1>0){
            *r2 = 0;
            return 2;
        }
        if (*r1==0){
            *r2 = 0;
            return 1;
        }
        if (*r1<0){
            *r2 = *r1;
            *r1 = 0;
            return 2;
        }
    }
    //quadratic case:
    det = a1*a1-4*a2*a0;
    //checking for the complex case
    if (det<0){
        return 0;
    }
    //checking for det = 0 case
    if (det==0){
        *r1 = -a1*(0.5/a2);
        *r2 = *r1;
        return 1;
    }
    //two distinct real roots (floating point version)
    if (a1<0){
        *r1 = (-a1+sqrtf(det))*(0.5/a2);
        *r2 = a0/(a2*(*r1));
        return 2;
    }
    else{
        *r2 = (-a1-sqrtf(det))*(0.5/a2);
        *r1 = a0/(a2*(*r2));
        return 2;
    }
}

//for solving quadratic equations for assessment (precise)
int quad_root_old(double a2, double a1, double a0, double * r1, double * r2){
    /*<ROZANOV>,<NIKOLAI>,<M3SC>*/
    //variable declaration
    int i;
    double det;
    // the linear case:
    if (a2==0){
        i = lin_root(a1,a0,r1);
        return i-2;
    }
    // a0 = 0 case:
    if(a0==0){
        i = lin_root(a2,a1,r1); // as a2 is != 0, we have that the linear equation always has one root

        //returning the value and roots the right way!
        if(*r1>0){
            *r2 = 0;
            return 2;
        }
        if (*r1==0){
            *r2 = 0;
            return 1;
        }
        if (*r1<0){
            *r2 = *r1;
            *r1 = 0;
            return 2;
        }
    }
    //quadratic case:
    det = a1*a1-4*a2*a0;
    //checking for the complex case
    if (det<0){
        return 0;
    }
    //checking for det = 0 case
    if (det==0){
        *r1 = -a1*(0.5/a2);
        *r2 = *r1;
        return 1;
    }
    //two distinct real roots (floating point version)
    if (a1<0){
        *r1 = (-a1+sqrt(det))*(0.5/a2);
        *r2 = a0/(a2*(*r1));
        return 2;
    }
    else{
        *r2 = (-a1-sqrt(det))*(0.5/a2);
        *r1 = a0/(a2*(*r2));
        return 2;
    }
}


/*
*
*
* QUAD_ROOT (ASSESSMENT)
*
*
*/


int quad_root(double a2, double a1, double a0, double * r1, double * r2){
    /*<ROZANOV>,<NIKOLAI>,<M3SC>*/
    /*****************************************************
    *variable declaration
    *****************************************************/
    int i;
    double det, pow_2=1.0;
    long int my_exponent, a2_mask, a1_mask, a0_mask, min_mask, max_mask;



    /****************************************************
    *the linear case:
    *****************************************************/

    if (a2==0){
        i = lin_root(a1,a0,r1);
        return i-2;
    }

    // a0 = 0 case:
    if(a0==0){
        i = lin_root(a2,a1,r1); // as a2 is != 0, we have that the linear equation always has one root

        //returning the value and roots the right way!
        if(*r1>0){
            *r2 = 0;
            return 2;
        }
        if (*r1==0){
            *r2 = 0;
            return 1;
        }
        if (*r1<0){
            *r2 = *r1;
            *r1 = 0;
            return 2;
        }
    }


    /****************************************************
    *the quadratic case:
    *****************************************************/
    if (a1==0){
        if(-a0/a2<0){
            return 0;
        }
        else{
            *r1 = sqrt(-a0/a2);//assumes input is sensible!
            *r2 = -(*r1);
            return 2;
        }
    }


    /***********************
    *Diskriminant
    ************************/

    //determining the exponent(and sign) of the coefficients

    a2_mask = ((*((long int *) &a2)) >> 52);//  //determines the exponent of a double
    a1_mask = ((*((long int *) &a1)) >> 52);//
    a0_mask = ((*((long int *) &a0)) >> 52);//

    /*
    *MASKS to make use of a2_mask, a1_mask, a0_mask
    *
    *04000 to determine sign bit
    *03777 get rid of the sign bit.
    *
    */

    //determining min/max of the exponent of the coefficients
    min_mask    = fmin((a2_mask & 03777),fmin(a1_mask & 03777,a0_mask & 03777)); //minimum exponent of a2,a1,a0
    max_mask    = fmax((a2_mask & 03777),fmax(a1_mask & 03777,a0_mask & 03777)); //maximum exponent of a2,a1,a0


    /*************
    *taking care of various computational cases to compute Diskriminant
    **************/
    if( max_mask>(1023+24) ){
        my_exponent = 2046-max_mask; // determining by how much to divide
        *((long int *) &pow_2) =  my_exponent<<52; //creating the divisor (such that double precision is preserved)

        //computing det, preventing overflow
        a2 = a2*pow_2;
        a1 = a1*pow_2;
        a0 = a0*pow_2;
        det = (a1*a1)-((double)4)*(a2*a0) ;

        if(det<0){ //checking for the complex case
            return 0;
        }

        det = sqrt(det)/pow_2;
    }
    else if( min_mask<(1023-24) && min_mask>(513) ){
        //my_exponent = 622+(1023-min_mask); // determining by how much to mulitply
        my_exponent = 1023-((1023-24)-min_mask); //determining by how much to divide
        *((long int *) &pow_2) =  my_exponent<<52;//creating the divisor (such that double precision is preserved)

        //computing det, taking into account underflow..
        a2 = a2*pow_2;
        a1 = a1*pow_2;
        a0 = a0*pow_2;
        det = (a1*a1)-((double)4)*(a2*a0) ;

        if(det<0){ //checking for the complex case
            return 0;
        }

        det = sqrt(det)/pow_2;
    }
    else{ //standard case
        det = a1*a1-((double)4)*a2*a0;

        if(det<0){ //checking for the complex case
            return 0;
        }

        else det = sqrt(det);
    }





    /****************************
    *solving the actual equation
    *****************************/

        //checking for det = 0 case
    if (det==0){
        *r1 = -a1/(((double)2) * a2);
        *r2 = *r1;
        return 1;
    }
    //two distinct real roots (floating point version)
    if (a1<0){
        *r1 = (-a1 + det*pow_2) /  (((double)2) * a2) ;
        *r2 = a0/(a2*(*r1));
        return 2;
    }
    else{
        *r2 = (-a1 - det*pow_2) / ( ((double)2) * a2);
        *r1 = a0/(a2*(*r2));
        return 2;
    }

}



/************************************************************************
Main Function
************************************************************************/
int main(void){

    /*******************************************************************
    ********************************************************************
    SOURCE for Radius:
        Charon: https://en.wikipedia.org/wiki/Charon_(moon)
        Pluto: https://en.wikipedia.org/wiki/Pluto
    ********************************************************************
    *******************************************************************/

    int i;
    char array_fields[8][16] = {"Pluto_dbl(km)\0","Pluto_trad(km)\0","Pluto_flt(km)\0","Pluto_flt2(km)\0","Charon_dbl(km)\0","Charon_trad(km)\0","Charon_flt(km)\0","Charon_flt2(km)\0"};
    //doubles
    double a2_=1,a1_,a0_;
    double Radius_pluto_dbl = 1187.0, Radius_charon_dbl=606.0, r1_dbl, r2_dbl;
    double d_vec_dbl[4] = {0.001,0.1,1.0,100.0};

    //floates
    float a2=1, a1, a0;
    float Radius_pluto_flt = 1187.0, Radius_charon_flt=606.0, r1_flt, r2_flt;
    float d_vec_flt[4] = {0.001,0.1,1.0,100.0};

    /*********************************************************
    //Section for my Information
    *********************************************************/

    // declaring the strings and i
    int my_int_for_info;
    char fields[5][14] = {"Name","CID","LIBRARY NO","Email Adress","Course CODE"};
    char values[5][33] = {"Rozanov, Nikolai","00831231","0247023908","nikolai.rozanov13@imperial.ac.uk", "M3SC"};

    //printing in automated way
    for(my_int_for_info = 0; my_int_for_info<5; my_int_for_info++){
        printf("%12s: <%s>\n",fields[my_int_for_info], values[my_int_for_info]);
    }
    printf("\n\n");



    /*********************************************************
    //Main Part of the Program
    *********************************************************/

    printf("%16s\t%16s\t%16s\t%16s\t%16s\t%16s\t%16s\t%16s\n",array_fields[0],array_fields[1],array_fields[2],array_fields[3],array_fields[4],array_fields[5],array_fields[6],array_fields[7]);


    for(i = 0; i<4; i++){

        //doubles Pluto
        a1_ = ((double) 2)*Radius_pluto_dbl;
        a0_ = -(d_vec_dbl[i]*d_vec_dbl[i]);
        //floates Pluto
        a1 = ((float) 2)*Radius_pluto_flt;
        a0 = -(d_vec_flt[i]*d_vec_flt[i]);
        //the roots
        quad_root_old(a2_,a1_,a0_,&r1_dbl,&r2_dbl);
        printf("%3.10e\t",r1_dbl);
        quad_root_traditional(a2_,a1_,a0_,&r1_dbl,&r2_dbl);
        printf("%3.10e\t",r1_dbl);
        quad_root_float(a2,a1,a0,&r1_flt,&r2_flt);
        printf("%3.10e\t",r1_flt);
        quad_root_float_traditional(a2,a1,a0,&r1_flt,&r2_flt);
        printf("%3.10e\t",r1_flt);


        //doubles Charon
        a1_ = ((double) 2)*Radius_charon_dbl;
        //floates Charon
        a1 = ((float) 2)*Radius_charon_flt;
        //the roots
        quad_root_old(a2_,a1_,a0_,&r1_dbl,&r2_dbl);
        printf("%3.10e\t",r1_dbl);
        quad_root_traditional(a2_,a1_,a0_,&r1_dbl,&r2_dbl);
        printf("%3.10e\t",r1_dbl);
        quad_root_float(a2,a1,a0,&r1_flt,&r2_flt);
        printf("%3.10e\t",r1_flt);
        quad_root_float_traditional(a2,a1,a0,&r1_flt,&r2_flt);
        printf("%3.10e\n",r1_flt);


    }

    return 0;
}




//
