/************************************************************************
Nikolai Rozanov,
Feb 2016,
00831231,
M3SC
************************************************************************/

#include <stdio.h>
#include <math.h>
#include <complex.h>




#define pi acos(-1);






/************************************************************************
************************************************************************
Equation Solvers Functions
************************************************************************
************************************************************************/

/***************************************************/
// for solving linear equations
/***************************************************/
int lin_root(double complex a1, double complex a0, double complex * r){
    //special cases
    if(a1==0){
        if(a0!=0){
            return -1;
        }
        return 0;
    }
    //standard cases
    *r = -a0/a1;
    return 1;
}



/***************************************************/
//for solving quadratic equations for assessment (precise)
/***************************************************/
int quad_root_complex(double complex a2, double complex a1, double complex a0, double complex * r1, double complex * r2){
    //variable declaration
    int i;
    double pow_2, temp;
    double complex det;
    double complex p, q;
    long int my_exponent;
    long int a2_real_mask, a2_imag_mask, a1_real_mask, a1_imag_mask, a0_real_mask, a0_imag_mask;
    long int min_mask_real,max_mask_real,min_mask_imag,max_mask_imag, min_mask, max_mask;



    // the linear case:
    if (a2==0){
        i = lin_root(a1,a0,r1);
        return i-2;
    }

    // a0 = 0 case:
    if(a0==0){
        i = lin_root(a2,a1,r1); // as a2 is != 0, we have that the linear equation always has one root

        //returning the value and roots the right way!
        if(creal(*r1)>0){
            *r2 = 0;
            return 2;
        }
        if (*r1==0){
            *r2 = 0;
            return 1;
        }
        if (creal(*r1)<0){
            *r2 = *r1;
            *r1 = 0;
            return 2;
        }
    }



    //quadratic cases
    //a1 ==0 case
    if(a1==0){
        *r1 = csqrt(-a0/a2);
        *r2 = -(*r1);
        return 2;
    }

    /***********************
    *Determinant
    ************************/

    //determining the exponent(and sign) of the coefficients (i.e bits 52-63)
    temp = creal(a2);
    a2_real_mask = ((*((long int *) &temp )) >> 52);//  //determines the exponent of a double
    temp = cimag(a2);
    a2_imag_mask = ((*((long int *) &temp )) >> 52);

    temp = creal(a1);
    a1_real_mask = ((*((long int *) &temp )) >> 52);//  //determines the exponent of a double
    temp = cimag(a1);
    a1_imag_mask = ((*((long int *) &temp )) >> 52);

    temp = creal(a0);
    a0_real_mask = ((*((long int *) &temp )) >> 52);//  //determines the exponent of a double
    temp = cimag(a0);
    a0_imag_mask = ((*((long int *) &temp )) >> 52);

    /*
    *Description of bit masks to make use of a2_mask, a1_mask, a0_mask
    *   (in base 8)
    * 04000 to determine sign bit
    * 03777 get rid of the sign bit. (i.e. determine exponent)
    *
    */

    //determining min/max
    min_mask_real    = fmin((a2_real_mask & 03777),fmin(a1_real_mask & 03777,a0_real_mask & 03777)); //minimum exponent of a2,a1,a0
    max_mask_real    = fmax((a2_real_mask & 03777),fmax(a1_real_mask & 03777,a0_real_mask & 03777)); //maximum exponent of a2,a1,a0

    min_mask_imag    = fmin((a2_imag_mask & 03777),fmin(a1_imag_mask & 03777,a0_imag_mask & 03777)); //minimum exponent of a2,a1,a0
    max_mask_imag    = fmax((a2_imag_mask & 03777),fmax(a1_imag_mask & 03777,a0_imag_mask & 03777)); //maximum exponent of a2,a1,a0

    min_mask = fmin(min_mask_real, min_mask_imag);
    max_mask = fmax(max_mask_real, max_mask_imag);
    /*
    *treating various cases
    */
    if( max_mask>(1023+24) ){ //exponent too high, prevents overflow

        my_exponent = 2046-max_mask; // determining by how much to divide
        *((long int *) &pow_2) =  my_exponent<<52; //creating the divisor

        //computing det, preventing overflow
        det = csqrt((a1*pow_2)*(a1*pow_2)-((double)4)*(a2*pow_2)*(a0*pow_2)) / pow_2;
    }
    else if( min_mask<(1023-24) ){ //numbers become too small.

        my_exponent = 1023-min_mask+1; // determining by how much to mulitply
        *((long int *) &pow_2) =  my_exponent<<52;

        //computing det, taking into account underflow..
        det = csqrt((a1*pow_2)*(a1*pow_2)-((double)4)*(a2*pow_2)*(a0*pow_2)) / pow_2;
    }
    else{ //standard case
        det = a1*a1-((double)4)*a2*a0;
        det = csqrt(det);
    }





    //checking for complex conjugate case:
    if (cimag(det)==0 && creal(det)<0){

        *r1 = (-a1 + det)*(0.5/a2);
        *r2 = conj(*r1);
        return 2;
    }
    //checking for det = 0 case
    if (det==0){
        *r1 = -a1*(0.5/a2);
        *r2 = *r1;
        return 1;
    }
    //two distinct roots
    if (creal(a1)*creal(sqrt(det))<0 && cimag(a1)*cimag(sqrt(det))<=0){ // checking for signs
        //printf("case1\n");
        *r1 = (-a1 + det)*(0.5/a2);
        *r2 = a0/(a2*(*r1));
        return 2;
    }
    else if(creal(a1)*creal(sqrt(det))>0 && cimag(a1)*cimag(sqrt(det))>=0){ //checking for signs
        //printf("case2\n");
        *r2 = (-a1 - det)*(0.5/a2);
        *r1 = a0/(a2*(*r2));
        return 2;
    }
    else{//case where one of the signs does not match
        //printf("case3\n");
        *r1 = (-a1 + det)*(0.5/a2);
        *r2 = a0/(a2*(*r1));
        return 2;
    }

}







/************************************************************************
************************************************************************
************************************************************************
Main Function
************************************************************************
************************************************************************
************************************************************************/
int main(void){


    int j;
    double complex a2, a1, a0, z, z2,r1,r2;

    /*********************************************************
    //Main Part of the Program
    *********************************************************/
    a2 = 12.0;

    for(j=0; j<81; j++){
        //declaration
        z   = cos(((double)j)*pi*0.025) + sin(((double)j)*pi*0.025)*I; //zj
        z2  = cos(((double)j)*pi*0.05) + sin(((double)j)*pi*0.05)*I; //zj^2
        a1  = -9.0*z2;
        a0  = 24.0*z - 8.0*z2 - 12.0;
        //solving and displaying
        if (quad_root_complex(a2,a1,a0,&r1,&r2)>=0){
            printf("%d,%lf,%lf,%lf,%lf\n",j,creal(r1),cimag(r1),creal(r2),cimag(r2));
        }
        else{
            printf("Error!\n");
        }
    }

    return 0;
}




//
