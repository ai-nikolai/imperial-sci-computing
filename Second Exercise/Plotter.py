"""
Nikolai Rozanov
December 2015
00831231
M3SC

Python script to plot
"""

import numpy as np
import matplotlib.pyplot as plt

#loading f
dat=np.array(np.loadtxt('complex.dat',delimiter=','))

x1 = np.transpose(dat)[1][:]
y1 = np.transpose(dat)[2][:]
x2 = np.transpose(dat)[3][:]
y2 = np.transpose(dat)[4][:]

plt.figure("Mastery")
plt.plot(x1, y1, color = (0.5,0.1,0.1), ls='.',marker = 'o')
plt.plot(x2, y2, color = (0.5,0.1,0.1), ls='.',marker = 'o')
plt.axis([-1.75,2.5,-1.5,1.5])
plt.xlabel('Real')
plt.ylabel('Imaginary')
plt.grid()
plt.title('Nikolai Rozanov, Complex Roots')
plt.show()
