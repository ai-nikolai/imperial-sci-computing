/************************************************************************
Nikolai Rozanov,
Feb 2016,
00831231,
M3SC
************************************************************************/

#include <stdio.h>
#include <math.h>
#include <complex.h>




#define pi acos(-1)






/************************************************************************
************************************************************************
Equation Solvers Functions
************************************************************************
************************************************************************/

/***************************************************/
// for solving linear equations
/***************************************************/
int lin_root(double complex a1, double complex a0, double complex * r){
    //special cases
    if(a1==0){
        if(a0!=0){
            return -1;
        }
        return 0;
    }
    //standard cases
    *r = -a0/a1;
    return 1;
}



/***************************************************/
//for solving quadratic equations for assessment (precise)
/***************************************************/
int quad_root_complex(double complex a2, double complex a1, double complex a0, double complex * r1, double complex * r2){
    int i;
    double complex det;
    // the linear case:
    if (a2==0){
        i = lin_root(a1,a0,r1);
        return i-2;
    }
    // a0 = 0 case:
    if(a0==0){
        i = lin_root(a2,a1,r1); // as a2 is != 0, we have that the linear equation always has one root
        //returning the value and roots the right way!
        if(creal(*r1)>0){
            *r2 = 0;
            return 2;
        }
        if (*r1==0){
            *r2 = 0;
            return 1;
        }
        if (creal(*r1)<0){
            *r2 = *r1;
            *r1 = 0;
            return 2;
        }
    }
    //quadratic cases
    //a1 ==0 case
    if(a1==0){
        *r1 = csqrt(-a0/a2);
        *r2 = -(*r1);
        return 2;
    }
    det = csqrt(a1*a1-((double)4)*a0*a2);
    //checking for complex conjugate case:
    if (cimag(det)==0 && creal(det)<0){

        *r1 = (-a1 + det)*(0.5/a2);
        *r2 = conj(*r1);
        return 2;
    }
    //checking for det = 0 case
    if (det==0){
        *r1 = -a1*(0.5/a2);
        *r2 = *r1;
        return 1;
    }
    //two distinct roots
    if (creal(a1)*creal(sqrt(det))<0 && cimag(a1)*cimag(sqrt(det))<=0){ // checking for signs
        //printf("case1\n");
        *r1 = (-a1 + det)*(0.5/a2);
        *r2 = a0/(a2*(*r1));
        return 2;
    }
    else if(creal(a1)*creal(sqrt(det))>0 && cimag(a1)*cimag(sqrt(det))>=0){ //checking for signs
        //printf("case2\n");
        *r2 = (-a1 - det)*(0.5/a2);
        *r1 = a0/(a2*(*r2));
        return 2;
    }
    else{//case where one of the signs does not match
        //printf("case3\n");
        *r1 = (-a1 + det)*(0.5/a2);
        *r2 = a0/(a2*(*r1));
        return 2;}   }






/************************************************************************
************************************************************************
************************************************************************
Main Function
************************************************************************
************************************************************************
************************************************************************/
int main(void){


    int j;
    double complex a2, a1, a0, z, z2,r1,r2;
    /*********************************************************
    //Section for my Information
    *********************************************************/
    // declaring the strings and i
    int my_int_for_info;
    char fields[5][14] = {"Name","CID","LIBRARY NO","Email Adress","Course CODE"};
    char values[5][33] = {"Rozanov,Nikolai","00831231","0247023908","nikolai.rozanov13@imperial.ac.uk", "M3SC"};
    //printing in automated way
    for(my_int_for_info = 0; my_int_for_info<5; my_int_for_info++){
        printf("%12s: <%s>\n",fields[my_int_for_info], values[my_int_for_info]);
    }
    printf("\n\n");
    /*********************************************************
    //Main Part of the Program
    *********************************************************/
    a2 = 12.0;

    for(j=0; j<81; j++){
        //declaration
        z   = cos(((double)j)*pi*0.025) + sin(((double)j)*pi*0.025)*I; //zj
        z2  = cos(((double)j)*pi*0.05) + sin(((double)j)*pi*0.05)*I; //zj^2
        a1  = -9.0*z2;
        a0  = 24.0*z - 8.0*z2 - 12.0;
        //solving and displaying
        if (quad_root_complex(a2,a1,a0,&r1,&r2)>=0){
            printf("%d,%lf,%lf,%lf,%lf\n",j,creal(r1),cimag(r1),creal(r2),cimag(r2));
        }
        else{
            printf("Error!\n");
        }
    }

    return 0;
}




//
