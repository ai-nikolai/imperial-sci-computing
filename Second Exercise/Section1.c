/************************************************************************
Nikolai Rozanov,
Feb 2016,
00831231,
M3SC
************************************************************************/

#include <stdio.h>
#include <math.h>



/************************************************************************
Helper Functions
************************************************************************/


// for solving linear equations
int lin_root(double a1, double a0, double * r){
/*<Rozanov>,<Nikolai>,<M3SC>*/
    //special cases
    if(a1==0){
        if(a0!=0){
            return -1;
        }
        return 0;
    }
    //standard cases
    *r = -a0/a1;
    return 1;
}


//for solving quadratic equations
int quad_root_traditional(double a2, double a1, double a0, double * r1, double * r2){
/*<Rozanov>,<Nikolai>,<M3SC>*/
    //variable declaration
    int i;
    double det;
    /*
    * the linear cases:
    */
    if (a2==0){
        i = lin_root(a1,a0,r1);
        return i-2;
    }
    // a0 = 0 case:
    if(a0==0){
        i = lin_root(a2,a1,r1);
        if(*r1>0){
            *r2 = 0;
            return 2;
        }
        if (*r1==0){
            *r2 = 0;
            return 1;
        }
        if (*r1<0){
            *r2 = *r1;
            *r1 = 0;
            return 2;
        }
    }
    /*
    *quadratic case:
    */
    det = a1*a1-4*a2*a0;
    //checking for the complex case
    if (det<0){
        return 0;
    }
    //checking for det = 0 case
    if (det==0){
        *r1 = -a1/(2*a2);
        *r2 = *r1;
        return 1;
    }
    //two distinct real roots
    *r1 = (-a1+sqrt(det))/(2*a2);
    *r2 = (-a1-sqrt(det))/(2*a2);
    return 2;
}

/************************************************************************
Main Function
************************************************************************/
int main(void){
    double a2,a1,a0, r1,r2;
    /*********************************************************
    //Section for my Information
    *********************************************************/
    // declaring the strings and i
    int my_int_for_info;
    char fields[5][14] = {"Name","CID","LIBRARY NO","Email Adress","Course CODE"};
    char values[5][33] = {"Rozanov, Nikolai","00831231","0247023908","nikolai.rozanov13@imperial.ac.uk", "M3SC"};

    //printing in automated way
    for(my_int_for_info = 0; my_int_for_info<5; my_int_for_info++){
        printf("%12s: <%s>\n",fields[my_int_for_info], values[my_int_for_info]);
    }
    printf("\n\n");
    /*********************************************************
    //Main Part of the Program
    *********************************************************/
    // getting the Coefficients
    printf("Enter the coefficients of the Quadratic Equation a2*x^2+a1*x+a0=0 \n");
    printf("In the order a2,a1,a0, separated by spaces\n");
    scanf("%lf %lf %lf",&a2,&a1,&a0);
    // printing the roots given the type of equation.
    switch(quad_root_traditional(a2,a1,a0,&r1,&r2)){
        case 2:
            printf("Two distinct real roots: x1 = %lf, x2 = %lf\n",r1, r2 );
            break;
        case 1:
            printf("One real root with double multiplicity: x = %lf \n", r1);
            break;
        case 0:
            printf("Two Complex roots \n");
            break;
        case -1:
            printf("One real root, with single multiplicity (linear equation): x = %lf\n", r1);
            break;
        case -2:
            printf("All values for x satisfy the equation!\n");
            break;
        case -3:
            printf("The equation has no solution!\n");
            break;
        default:
            printf("Error in the code, this case does not exist! \n");
    }
    return 0;
}




//
