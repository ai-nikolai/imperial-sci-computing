/************************************************************************
Nikolai Rozanov,
Feb 2016,
00831231,
M3SC
************************************************************************/

#include <stdio.h>
#include <math.h>
#include <complex.h>

/************************************************************************
Function Declarations
************************************************************************/
int lin_root(double *, double *);
int quad_root(double *, double *);
int rcubic_roots(double *, double *);


/************************************************************************
Helper Functions
************************************************************************/
double evaluator(double a2, double a1, double a0, double root){
    return (root*root*root + a2*root*root + a1*root + a0);
}
double complex complex_evaluator(double a2, double a1, double a0, double real_, double imag_){
    double complex root = real_ + imag_*I;
    return (root*root*root + a2*root*root + a1*root + a0);
}

/************************************************************************
Main Function
************************************************************************/
int main(void){
    int k, not_done=0x1, counter=0;;
    double a[3], r[4];
    double complex result;
    /*********************************************************
    //Section for my Information
    *********************************************************/
    // declaring the strings and i
    int my_int_for_info;
    char fields[5][14] = {"Name","CID","LIBRARY NO","Email Adress","Course CODE"};
    char values[5][33] = {"Rozanov, Nikolai","00831231","0247023908","nikolai.rozanov13@imperial.ac.uk", "M3SC"};

    //printing in automated way
    for(my_int_for_info = 0; my_int_for_info<5; my_int_for_info++){
        printf("%12s: <%s>\n",fields[my_int_for_info], values[my_int_for_info]);
    }
    printf("%12s: <%s>\n","Date:", __DATE__);
    printf("%12s: <%s>\n","Time:", __TIME__);

    printf("\n\n");
    /*********************************************************
    //Main Part of the Program
    *********************************************************/
    // getting the Coefficients
    //printf("Enter the coefficients of the Quadratic Equation a2*x^2+a1*x+a0=0 \n");
    //printf("In the order a2,a1,a0, separated by spaces\n");
    while(not_done){
        counter++;
        scanf("%d %lf %lf %lf",&not_done,&a[2],&a[1],&a[0]);
        printf("\n\nCase:%d a[2]=%lf a[1] = %lf a[0]=%lf\n", counter, a[2],a[1],a[0]);
        // printing the roots given the type of equation.
        switch((k=rcubic_roots(a,r))){
            case 3:
                printf("Three distinct real roots: r[1] = %lf, r[2] = %lf, r[3] = %lf\n",r[1], r[2], r[3]);
                break;
            case 2:
                printf("One real root with double multiplicity, one with single: r[1] = %lf r[2] = %lf\n", r[1],r[2]);
                break;
            case 1:
                printf("One real root with triple multiplicity: r[1] = %lf \n", r[1]);
                    break;
            case 0:
                printf("One real root and complex conjugate pair: r[1] = %lf, r_real = %lf r_imag = %lf\n", r[1], r[2] ,r[3]);
                break;
            default:
                printf("Error in the code, this case does not exist! \n");
        }
        if(k!=0){
            printf("This is how these roots perform: %lf %lf %lf\n", evaluator(a[2],a[1],a[0],r[1]),evaluator(a[2],a[1],a[0],r[2]),evaluator(a[2],a[1],a[0],r[3]) );
        }
        if(k==0){
            printf("This is how these roots perform: %lf\n", evaluator(a[2],a[1],a[0],r[1]) );
            result = complex_evaluator(a[2],a[1],a[0],r[2],r[3]);
            printf("This is how these roots perform: %lf + %lfi\n", creal(result), cimag(result) );
        }
        printf("End of Case:%d\n", counter);

    }

    return 0;
}
