/************************************************************************
Nikolai Rozanov,
March 2016,
00831231,
M3SC
************************************************************************/
#include <stdio.h>
#include <math.h>


int lin_root(double * a, double * r){
    /*<ROZANOV>,<NIKOLAI>,<M3SC>*/
    //special cases
    if(a[1]==0){
        if(a[0]!=0){
            return -1;
        }
        return 0;
    }
    //standard cases
    r[1] = -a[0]/a[1];
    return 1;
}
