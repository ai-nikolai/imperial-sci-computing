/************************************************************************
Nikolai Rozanov,
March 2016,
00831231,
M3SC
************************************************************************/
#include <stdio.h>
#include <math.h>

#define max(x,y) (((x)>(y)) ? (x) : (y))
#define ABS_(x) (  ((x)>=0) ? (x) : (-(x))  )


int lin_root(double *, double *);
int quad_root(double *, double *);
int rcubic_roots(double *, double *);





double pow_2_(long int exp){
    /*<ROZANOV>,<NIKOLAI>,<M3SC>*/
    double divisor;
    *((long int *) &divisor) =  exp<<52;
    return divisor;
}



int quad_root(double * a, double * r){
    /*<ROZANOV>,<NIKOLAI>,<M3SC>*/
    /*****************************************************
     *variable declaration
     *****************************************************/
    int i;
    double det, pow_2=1.0;
    long int my_exponent=0, a2_mask, a1_mask, a0_mask, min_mask, max_mask;
    double a2,a1,a0;
    double temp_coefficients[2];

    a2 = a[2];
    a1 = a[1];
    a0 = a[0];

    /****************************************************
     *the linear case:
     *****************************************************/

    if (a2==0){
        printf("case 1 %lf %lf\n",a0,a1);
        temp_coefficients[0]=a0;
        temp_coefficients[1]=a1;
        i = lin_root(temp_coefficients,r);
        return i-2;
    }

    // a0 = 0 case:
    if(a0==0){
        printf("case 2 %lf %lf\n",a0,a1);
        temp_coefficients[0]=a1;
        temp_coefficients[1]=a2;
        i = lin_root(temp_coefficients,r+1); // as a2 is != 0, we have that the linear equation always has one root

        //returning the value and roots the right way!
        if(r[2]<0){
            r[1] = 0;
            return 2;
        }
        if (r[2]==0){
            r[1] = 0;
            return 1;
        }
        if (r[2]>0){
            r[1] = r[2];
            r[2] = 0;
            return 2;
        }
    }


    /****************************************************
     *the quadratic case:
     *****************************************************/
    if (a1==0){
        if(-a0/a2<0){
            r[1] = 0;
            r[2] = sqrt(a0/a2);
            return 0;
        }
        else{
            r[2] = -sqrt(-a0/a2);
            r[1] = -(r[2]);
            return 2;
        }
    }



    /***********************
     *Diskriminant
     ************************/

    //determining the exponent(and sign) of the coefficients

    a2_mask = ((*((long int *) &a2)) >> 52);//  //determines the exponent of a double
    a1_mask = ((*((long int *) &a1)) >> 52);//
    a0_mask = ((*((long int *) &a0)) >> 52);//

    /*
     *MASKS to make use of a2_mask, a1_mask, a0_mask
     *
     *04000 to determine sign bit
     *03777 get rid of the sign bit.
     *
     */

    //determining min/max of the exponent of the coefficients
    min_mask    = fmin((a2_mask & 03777),fmin(a1_mask & 03777,a0_mask & 03777)); //minimum exponent of a2,a1,a0
    max_mask    = fmax((a2_mask & 03777),fmax(a1_mask & 03777,a0_mask & 03777)); //maximum exponent of a2,a1,a0



    if( max_mask>(1023+24) || (max_mask-min_mask>=1023) ){
        my_exponent += 2046-max_mask; // determining by how much to divide
        *((long int *) &pow_2) =  my_exponent<<52; //creating the divisor (such that double precision is preserved)

    }
    else if( min_mask<(1023-24) ){
        my_exponent += 2046-min_mask;
        *((long int *) &pow_2) =  my_exponent<<52;//creating the divisor (such that double precision is preserved)
    }



    //
    a2 = a2*pow_2;
    a1 = a1*pow_2;
    a0 = a0*pow_2;
    det = (a1*a1)-((double)4)*(a2*a0) ;

    if(det<0){ //checking for the complex case
        //printf("Case5\n");
        if(((a2_mask & 03777)<(1023-200)) && (my_exponent<(1023-200))){
            r[1] = -a[1]/(((double)2) * a[2]);
            r[2] = sqrt(-det)/(((double)2) * ABS_(a[2]));
            return 0;
        }
        r[1] = -a1/(((double)2) * a2);
        r[2] = sqrt(-det)/(((double)2) * ABS_(a2));
        return 0;
    }

    det = sqrt(det);


    /****************************
     *solving the actual equation
     *****************************/

    //checking for det = 0 case
    if (det==0){
        //printf("Case4\n");
        if(((a2_mask & 03777)<(1023-200)) && (my_exponent<(1023-200))){
            r[2] = -a[1]/(((double)2) * a[2]);
            r[1] = r[2];
            return 1;
        }
        r[2] = -a1/(((double)2) * a2);
        r[1] = r[2];
        return 1;
    }


    //two distinct real roots (floating point version)
    if (a1<0){
        if(((a2_mask & 03777)<(1023-200)) && (my_exponent<(1023-200))){
            r[1] = (-a[1] + det) /  (((double)2) * a[2]) ;
        }
        else{
             r[1] = (-a1 + det) /  (((double)2) * a2) ;
         }
    }
    else{
        if(((a2_mask & 03777)<(1023-200)) && (my_exponent<(1023-200))){
            r[1] = -(a[1] + det) /  (((double)2) * a[2]) ;
        }
        else{
            r[1] = -(a1 + det) /  (((double)2) * a2);
        }
    }


    //getting the other root.

    if (ABS_((r[1]))>pow_2_(1023-26)){
        if(((a2_mask & 03777)<(1023-200)) && (my_exponent<(1023-200))){
            r[2] = (a[0]/a[2])/(r[1]);
        }
        else{
            r[2] = (a0/a2)/(r[1]);
        }
    }

    else if(a1<0){
        r[2] = -(a1 + det) /  (((double)2) * a2);
    }
    else{
        r[2] = (-a1 + det) /  (((double)2) * a2);
    }



    //ordering roots
    if ((r[2])>(r[1])){
        det = (r[1]);
        (r[1]) = (r[2]);
        (r[2]) = det;
    }

    return 2;


}
