/************************************************************************
Nikolai Rozanov,
Feb 2016,
00831231,
M3SC
************************************************************************/

#include <stdio.h>
#include <math.h>

#define pi (acos(-1))
/************************************************************************
Function Declarations
************************************************************************/
int lin_root(double *, double *);
int quad_root(double *, double *);
int rcubic_roots(double *, double *);
int rquartic_roots(double *, double *);

/************************************************************************
Helper Functions
************************************************************************/
void get_coefficients(double * a, double b, double X, double Y){
    a[0] = -1;
    a[1] = (2*X -2 + 2*b*b)/(b*Y);
    a[2] = 0;
    a[3] = (2*X +2 - 2*b*b)/(b*Y);
}

double to_degree(double rad){
    return rad/pi/2*360;
}

/************************************************************************
Main Function
************************************************************************/
int main(void){
    int k;
    double a[4], r[5];
    double polar, equator, b, X, Y;
    double b_max;
    /*********************************************************
    //Section for my Information
    *********************************************************/
    // declaring the strings and i
    int my_int_for_info;
    char fields[5][14] = {"Name","CID","LIBRARY NO","Email Adress","Course CODE"};
    char values[5][33] = {"Rozanov, Nikolai","00831231","0247023908","nikolai.rozanov13@imperial.ac.uk", "M3SC"};

    //printing in automated way
    for(my_int_for_info = 0; my_int_for_info<5; my_int_for_info++){
        printf("%12s: <%s>\n",fields[my_int_for_info], values[my_int_for_info]);
    }
    printf("%12s: <%s>\n","Date:", __DATE__);
    printf("%12s: <%s>\n","Time:", __TIME__);

    printf("\n\n");
    /*********************************************************
    //Main Part of the Program
    *********************************************************/
    X = 0.375;
    Y = 0.5;
    b_max = 0.05;

    for(b=0.05;b<1;b+=0.05){
        get_coefficients(a,b,X,Y);
        k = rquartic_roots(a,r);
        if(k==4){
            b_max = b;
        }
    }
    get_coefficients(a,b_max,X,Y);
    k = rquartic_roots(a,r);
    printf("The max b is: %3.2lf and the angles are: %lf %lf %lf %lf",b_max, atan(r[1])*2,atan(r[2])*2,atan(r[3])*2,atan(r[4])*2);

    return 0;
}
