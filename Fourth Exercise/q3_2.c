/************************************************************************
Nikolai Rozanov,
March 2016,
00831231,
M3SC
************************************************************************/

#include <stdio.h>
#include <math.h>

#define pi (acos(-1))
/************************************************************************
Function Declarations
************************************************************************/
int lin_root(double *, double *);
int quad_root(double *, double *);
int rcubic_roots(double *, double *);
int rquartic_roots(double *, double *);

/************************************************************************
Helper Functions
************************************************************************/
void get_coefficients(double * a, double b, double X, double Y){
    a[0] = -1;
    a[1] = (2*X -2 + 2*b*b)/(b*Y);
    a[2] = 0;
    a[3] = (2*X +2 - 2*b*b)/(b*Y);
}

double to_degree(double rad){
    return rad/pi/2*360;
}

/************************************************************************
Main Function
************************************************************************/
int main(void){
    int k;
    double a[4], r[5];
    double polar, equator, b, X, Y,X2, Y2, norm, theta[4];
    double b_max;
    /*********************************************************
    //Section for my Information
    *********************************************************/
    // declaring the strings and i
    int my_int_for_info;
    char fields[5][14] = {"Name","CID","LIBRARY NO","Email Adress","Course CODE"};
    char values[5][33] = {"Rozanov, Nikolai","00831231","0247023908","nikolai.rozanov13@imperial.ac.uk", "M3SC"};

    //printing in automated way
    for(my_int_for_info = 0; my_int_for_info<5; my_int_for_info++){
        printf("%12s: <%s>\n",fields[my_int_for_info], values[my_int_for_info]);
    }
    printf("%12s: <%s>\n","Date:", __DATE__);
    printf("%12s: <%s>\n","Time:", __TIME__);

    printf("\n\n");
    /*********************************************************
    //Main Part of the Program
    *********************************************************/
    X = 4980265.80632;
    Y = 4950022.89191;
    polar = 6356752.0 ;
    equator =  6378137.0 ;

    b = polar/equator;
    X2 = X/equator;
    Y2 = Y/equator;

    get_coefficients(a,b,X2,Y2);
    k = rquartic_roots(a,r);

    X2 = X-equator*cos(atan(r[1])*2);
    Y2 = Y-polar * sin(atan(r[1])*2);
    norm = sqrt(X2*X2 + Y2*Y2);

    printf("The distance is %3.0lfm and the latitude is: %lf ",norm, to_degree(atan(Y2/X2)));

    return 0;
}
