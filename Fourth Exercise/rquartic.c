/************************************************************************
Nikolai Rozanov,
March 2016,
00831231,
M3SC
************************************************************************/
#include <stdio.h>
#include <math.h>
#include <complex.h>



/************************************************************************
Function Declarations
************************************************************************/
#define pi (acos(-1))
#define max(x,y) (((x)>(y)) ? (x) : (y))
#define ABS_(x) (  ((x)>=0) ? (x) : (-(x))  )
#define sgn(x) ( ((x)>=0) ? (1) : (-1)  )


int lin_root(double *, double *);
int quad_root(double *, double *);
int rcubic_roots(double *, double *);
void min_max(double *, double * );


/************************************************************************
Helper Functions
************************************************************************/
double my_qroot(double x){
    return sqrt(sqrt(x));
}



void root_order_(double * r, int num){



    switch(num){
        case 4:
        case 3:
        case 1:
            min_max(r+1,r+4);
            min_max(r+1,r+3);
            min_max(r+1,r+2);
            min_max(r+2,r+4);
            min_max(r+2,r+3);
            min_max(r+3,r+4);
            break;

        case 2:
            min_max(r+1,r+2);
            break;
        default:
            printf("This case should not exist\n");
            break;
    }


}





/************************************************************************
Main Function
************************************************************************/

int rquartic_roots(double * a, double * r){
    /*<ROZANOV>,<NIKOLAI>,<M3SC>*/
    /*****************************************************
    *variable declaration
    *****************************************************/
    int i, k,k1,k2;
    double det, pow_2=1.0, zero = 0, one = 1, two = 2, three = 3,four = 4, nine =9,eleven = 11;
    double complex temp1, temp2;
    double benchmark = 4.0e-3;
    double a3, a2, a1, a0, root;
    double temp_coefs[3], p1,p0,q1,q0;
    double temp_roots[2];


    a3 = a[3];
    a2 = a[2];
    a1 = a[1];
    a0 = a[0];


    /***************************************************
    * Special Cases
    ***************************************************/

    if((a0==0)&&(a1==0)&&(a2==0)&&(a3==0)){
        r[1]=0;
        r[2]=0;
        r[3]=0;
        r[4]=0;
        return 0;
    }
    //Q2(iii)
    if((a1==0)&&(a2==0)&&(a3==0)){
        if(a0<0){
            r[1]=my_qroot(-a0);
            r[2]=-r[1];
            r[3]=0;
            r[4]=r[1];
            return 2;
        }
        else{
            temp1 = sqrt(a0)*I;
            temp1 = csqrt(temp1);
            r[1] = creal(temp1);
            r[2] = cimag(temp1);
            r[3] = -creal(temp1);
            r[4] = cimag(temp1);
            return 0;
        }
    }

    if((a0==0)&&(a1==0)&&(a2==0)){
        r[2]=0;
        r[3]=0;
        r[4]=0;
        r[1]=-a3;
        root_order_(r,4);
        return 1;
    }

    if((a0==0)&&(a1==0)){
        temp_coefs[0]=a2;
        temp_coefs[1]=a3;
        temp_coefs[2]=one;
        k = quad_root(a,r+2);
        r[1] = 0;
        r[2] = 0;
        if(k==0){
            return 0;
        }
        else if(k==2){
            root_order_(r,4);
            return 3;
        }
        else{
            root_order_(r,4);
            return 1;
        }
    }
    //Q2(i)
    if(a0==0){
        temp_coefs[0]=a1;
        temp_coefs[1]=a2;
        temp_coefs[2]=a3;
        r[1] = 0;
        k = rcubic_roots(r+1,a);
        if(k==0){
            root_order_(r,2);
            return 2; //even when r[0]==r[1]
        }
        else{
            root_order_(r,4);
            return ((k==1) ? 1 : k+1);
        }

    }
    //Q2(ii)
    if((a3==0)&&(a1==0)){
        temp_coefs[0]=a0;
        temp_coefs[1]=a2;
        temp_coefs[2]=one;
        k = quad_root(temp_coefs,temp_roots-1);
        if (k==0){
            temp1 = temp_roots[0] + temp_roots[1]*I;
            temp2 = temp_roots[0] + temp_roots[1]*I;
            temp1 = csqrt(temp1);
            temp2 = csqrt(temp2);
            r[1] = creal(temp1);
            r[2] = cimag(temp1);
            r[3] = creal(temp2);
            r[4] = cimag(temp2);
            return 0;
        }
        else{
            if (temp_roots[0]<0 || temp_roots[1]<0){
                if(temp_roots[0]<0 && temp_roots[1]>=0){
                    r[1] = sqrt(temp_roots[1]);
                    r[2] = -r[1];
                    r[3] = 0;
                    r[4] = sqrt(-temp_roots[0]);
                    return 2;
                }
                else if(temp_roots[1]<0 && temp_roots[0]>=0){
                    r[1] = sqrt(temp_roots[0]);
                    r[2] = -r[1];
                    r[3] = 0;
                    r[4] = sqrt(-temp_roots[1]);
                    return 2;
                }
                else{
                    r[1] = 0;
                    r[2] = sqrt(-temp_roots[0]);
                    r[3] = 0;
                    r[4] = sqrt(-temp_roots[1]);
                    return 0;
                }
            }
            else{
                r[1] = sqrt(temp_roots[0]);
                r[2] = -r[1];
                r[3] = sqrt(temp_roots[1]);
                r[4] = -r[3];
                if(r[1]==r[3]){
                    min_max(r+1,r+2);
                    return 1;
                }
                else{
                    root_order_(r,4);
                    return 4;
                }
            }
        }
    }

    /***************************************************
    * Standard Quartic case
    ***************************************************/
    temp_coefs[0]= four * a0*a2 - a1*a1 - a0*a3*a3;
    temp_coefs[1]= a1*a3 - four*a0;
    temp_coefs[2]= -a2;

    //solving the auxiliary cubic equation.
    rcubic_roots(temp_coefs,r);
    //getting the largest root
    root = r[1];


    temp_coefs[0]= root/2 - sqrt(root*root/4-a0);
    temp_coefs[1]= a3/two - sqrt(a3*a3/4+root-a2);


    //determining which quadratics to solve
    p0 = root/2 + sqrt(root*root/4-a0);
    p1 = a3/two + sqrt(a3*a3/4+root-a2);
    q0 = root/2 - sqrt(root*root/4-a0);
    q1 = a3/two - sqrt(a3*a3/4+root-a2);

    //printf("%3.17lf\n",p1*q0+q1*p0);
    if(ABS_(p1*q0+q1*p0-a1)>benchmark){
        //printf("other\n");
        p0 = root/2 - sqrt(root*root/4-a0);
        p1 = a3/two + sqrt(a3*a3/4+root-a2);
        q0 = root/2 + sqrt(root*root/4-a0);
        q1 = a3/two - sqrt(a3*a3/4+root-a2);
    }
    //solving for the first two roots
    temp_coefs[0]= p0;
    temp_coefs[1]= p1;
    temp_coefs[2]= one;

    k1 = quad_root(temp_coefs,r+2);

    //solving for the remaining roots
    temp_coefs[0]= q0;
    temp_coefs[1]= q1;
    k2 = quad_root(temp_coefs,r);

    //taking care of different
    if(k1==0 || k2 ==0){
        if(k1==k2){
            return 0;
        }
        else if(k2==0){
            //printf("case1\n");
            temp_coefs[0] = r[1];
            temp_coefs[1] = r[2];
            r[1] = r[3];
            r[2] = r[4];
            r[3] = temp_coefs[0];
            r[4] = temp_coefs[1];
            return 2;
        }
        else{
            //printf("case2 %d %d\n", k1, k2);
            return 2;
        }
    }
    root_order_(r,4);
    return 4-((k1%2)+(k2%2)+((k2%2)*(k1%2))); //always getting the correct return


}
