/************************************************************************
Nikolai Rozanov,
Feb 2016,
00831231,
M3SC
************************************************************************/

#include <stdio.h>
#include <math.h>
#include <complex.h>

/************************************************************************
Function Declarations
************************************************************************/
int lin_root(double,double,double *);
int quad_root(double, double, double,double *, double *);
int rcubic_roots(double , double , double , double *, double *, double *);


/************************************************************************
Helper Functions
************************************************************************/
double evaluator(double a2, double a1, double a0, double root){
    return (root*root*root + a2*root*root + a1*root + a0);
}
double complex complex_evaluator(double a2, double a1, double a0, double real_, double imag_){
    double complex root = real_ + imag_*I;
    return (root*root*root + a2*root*root + a1*root + a0);
}

/************************************************************************
Main Function
************************************************************************/
int main(void){
    int k, not_done=0x1, counter=0;;
    double a2,a1,a0, r1,r2, r3;
    double complex result;
    /*********************************************************
    //Section for my Information
    *********************************************************/
    // declaring the strings and i
    int my_int_for_info;
    char fields[5][14] = {"Name","CID","LIBRARY NO","Email Adress","Course CODE"};
    char values[5][33] = {"Rozanov, Nikolai","00831231","0247023908","nikolai.rozanov13@imperial.ac.uk", "M3SC"};

    //printing in automated way
    for(my_int_for_info = 0; my_int_for_info<5; my_int_for_info++){
        printf("%12s: <%s>\n",fields[my_int_for_info], values[my_int_for_info]);
    }
    printf("\n\n");
    /*********************************************************
    //Main Part of the Program
    *********************************************************/
    // getting the Coefficients
    //printf("Enter the coefficients of the Quadratic Equation a2*x^2+a1*x+a0=0 \n");
    //printf("In the order a2,a1,a0, separated by spaces\n");
    while(not_done){
        counter++;
        scanf("%d %lf %lf %lf",&not_done,&a2,&a1,&a0);
        printf("\n\nCase:%d a2=%lf a1 = %lf a0=%lf\n", counter, a2,a1,a0);
        // printing the roots given the type of equation.
        switch((k=rcubic_roots(a2,a1,a0,&r1,&r2, &r3))){
            case 3:
                printf("Three distinct real roots: r1 = %lf, r2 = %lf, r3 = %lf\n",r1, r2, r3);
                break;
            case 2:
                printf("One real root with double multiplicity, one with single: r1 = %lf r2 = %lf\n", r1,r2);
                break;
            case 1:
                printf("One real root with triple multiplicity: r1 = %lf \n", r1);
                    break;
            case 0:
                printf("One real root and complex conjugate pair: r1 = %lf, r_real = %lf r_imag = %lf\n", r1, r2 ,r3);
                break;
            default:
                printf("Error in the code, this case does not exist! \n");
        }
        if(k!=0){
            printf("This is how these roots perform: %lf %lf %lf\n", evaluator(a2,a1,a0,r1),evaluator(a2,a1,a0,r2),evaluator(a2,a1,a0,r3) );
        }
        if(k==0){
            printf("This is how these roots perform: %lf\n", evaluator(a2,a1,a0,r1) );
            result = complex_evaluator(a2,a1,a0,r2,r3);
            printf("This is how these roots perform: %lf + %lfi\n", creal(result), cimag(result) );
        }
        printf("End of Case:%d\n", counter);

    }

    return 0;
}
