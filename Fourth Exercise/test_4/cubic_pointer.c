/************************************************************************
Nikolai Rozanov,
March 2016,
00831231,
M3SC
************************************************************************/
#include <stdio.h>
#include <math.h>



/************************************************************************
Function Declarations
************************************************************************/
#define pi (acos(-1))
#define max(x,y) (((x)>(y)) ? (x) : (y))
#define ABS_(x) (  ((x)>=0) ? (x) : (-(x))  )
#define sgn(x) ( ((x)>=0) ? (1) : (-1)  )


int lin_root(double *, double *);
int quad_root(double *, double *);
int rcubic_roots(double *, double *);



/************************************************************************
Helper Functions
************************************************************************/

void min_max(double * x, double * y){
    double temp;
    if ((*x)<(*y)){
        temp = *x;
        *x = *y;
        *y = temp;
    }
}

void root_order(double * r1, double *r2, double *r3){
    min_max(r1,r2);
    min_max(r2,r3);
    min_max(r1,r2);
}

double newton(double y0, double p, double benchmark){
    double three = 3, one = 1;
    while (ABS_(y0*y0*y0 -p*y0 -1)>benchmark && ABS_(y0*y0*y0 -p*y0 -1)<10){
        y0= y0 - (y0*y0*y0 -p*y0 - one)/(three*y0*y0 -p);
    }
    return y0;
}

double getP(double a2, double a1, double a, double b){
    double p, two = 2,three = 3;
    p = -(three*b*b/a/a +  a2*two*b/a/a + a1/a/a);
    return p;
}

double getA(double a2, double a1, double a0){
    double two = 2,three = 3, nine = 9;
    return cbrt(-(two * a2*a2*a2 - nine*a2*a1 + three*nine*a0 ))/three;
}






double pow_2_(long int exp){
    /*<ROZANOV>,<NIKOLAI>,<M3SC>*/
    double divisor;
    *((long int *) &divisor) =  exp<<52;
    return divisor;
}

int lin_root(double * a, double * r){
    /*<ROZANOV>,<NIKOLAI>,<M3SC>*/
    //special cases
    if(a[1]==0){
        if(a[0]!=0){
            return -1;
        }
        return 0;
    }
    //standard cases
    r[1] = -a[0]/a[1];
    return 1;
}




 int quad_root(double * a, double * r){
     /*<ROZANOV>,<NIKOLAI>,<M3SC>*/
     /*****************************************************
      *variable declaration
      *****************************************************/
     int i;
     double det, pow_2=1.0;
     long int my_exponent=0, a2_mask, a1_mask, a0_mask, min_mask, max_mask;
     double a2,a1,a0;
     double temp_coefficients[2];

     a2 = a[2];
     a1 = a[1];
     a0 = a[0];

     /****************************************************
      *the linear case:
      *****************************************************/

     if((a1==0) && (a0==0)){
         r[1]=0;
         r[2]=0;
         return 1;
     }

     if (a2==0){
         temp_coefficients[0]=a0;
         temp_coefficients[1]=a1;
         i = lin_root(temp_coefficients,r);
         return i-2;
     }

     // a0 = 0 case:
     if(a0==0){
         temp_coefficients[0]=a1;
         temp_coefficients[1]=a2;
         i = lin_root(temp_coefficients,r+1); // as a2 is != 0, we have that the linear equation always has one root

         //returning the value and roots the right way!
         if(r[2]<0){
             r[1] = 0;
             return 2;
         }
         if (r[2]==0){
             r[1] = 0;
             return 1;
         }
         if (r[2]>0){
             r[1] = r[2];
             r[2] = 0;
             return 2;
         }
     }


     /****************************************************
      *the quadratic case:
      *****************************************************/
     if (a1==0){
         if(-a0/a2<0){
             r[2] = 0;
             r[1] = sqrt(a0/a2);
             return 0;
         }
         else{
             r[2] = -sqrt(-a0/a2);
             r[1] = -(r[2]);
             return 2;
         }
     }



     /***********************
      *Diskriminant
      ************************/

     //determining the exponent(and sign) of the coefficients

     a2_mask = ((*((long int *) &a2)) >> 52);//  //determines the exponent of a double
     a1_mask = ((*((long int *) &a1)) >> 52);//
     a0_mask = ((*((long int *) &a0)) >> 52);//

     /*
      *MASKS to make use of a2_mask, a1_mask, a0_mask
      *
      *04000 to determine sign bit
      *03777 get rid of the sign bit.
      *
      */

     //determining min/max of the exponent of the coefficients
     min_mask    = fmin((a2_mask & 03777),fmin(a1_mask & 03777,a0_mask & 03777)); //minimum exponent of a2,a1,a0
     max_mask    = fmax((a2_mask & 03777),fmax(a1_mask & 03777,a0_mask & 03777)); //maximum exponent of a2,a1,a0



     if( max_mask>(1023+24) || (max_mask-min_mask>=1023) ){
         my_exponent += 2046-max_mask; // determining by how much to divide
         *((long int *) &pow_2) =  my_exponent<<52; //creating the divisor (such that double precision is preserved)

     }
     else if( min_mask<(1023-24) ){
         my_exponent += 2046-min_mask;
         *((long int *) &pow_2) =  my_exponent<<52;//creating the divisor (such that double precision is preserved)
     }



     //
     a2 = a2*pow_2;
     a1 = a1*pow_2;
     a0 = a0*pow_2;
     det = (a1*a1)-((double)4)*(a2*a0) ;

     if(det<0){ //checking for the complex case
         //printf("Case5\n");
         if(((a2_mask & 03777)<(1023-200)) && (my_exponent<(1023-200))){
             r[1] = -a[1]/(((double)2) * a[1]);
             r[2] = sqrt(-det)/(((double)2) * ABS_(a[2]));
             return 0;
         }
         r[1] = -a1/(((double)2) * a2);
         r[2] = sqrt(-det)/(((double)2) * ABS_(a2));
         return 0;
     }

     det = sqrt(det);


     /****************************
      *solving the actual equation
      *****************************/

     //checking for det = 0 case
     if (det==0){
         //printf("Case4\n");
         if(((a2_mask & 03777)<(1023-200)) && (my_exponent<(1023-200))){
             r[2] = -a[1]/(((double)2) * a[2]);
             r[1] = r[2];
             return 1;
         }
         r[2] = -a1/(((double)2) * a2);
         r[1] = r[2];
         return 1;
     }


     //two distinct real roots (floating point version)
     if (a1<0){
         if(((a2_mask & 03777)<(1023-200)) && (my_exponent<(1023-200))){
             r[1] = (-a[1] + det) /  (((double)2) * a[2]) ;
         }
         else{
              r[1] = (-a1 + det) /  (((double)2) * a2) ;
          }
     }
     else{
         if(((a2_mask & 03777)<(1023-200)) && (my_exponent<(1023-200))){
             r[1] = -(a[1] + det) /  (((double)2) * a[2]) ;
         }
         else{
             r[1] = -(a1 + det) /  (((double)2) * a2);
         }
     }


     //getting the other root.

     if (ABS_((r[1]))>pow_2_(1023-26)){
         if(((a2_mask & 03777)<(1023-200)) && (my_exponent<(1023-200))){
             r[2] = (a[0]/a[2])/(r[1]);
         }
         else{
             r[2] = (a0/a2)/(r[1]);
         }
     }

     else if(a1<0){
         r[2] = -(a1 + det) /  (((double)2) * a2);
     }
     else{
         r[2] = (-a1 + det) /  (((double)2) * a2);
     }



     //ordering roots
     if ((r[2])>(r[1])){
         det = (r[1]);
         (r[1]) = (r[2]);
         (r[2]) = det;
     }

     return 2;


 }




/************************************************************************
Main Function
************************************************************************/

int rcubic_roots(double * a, double * r){
    /*<ROZANOV>,<NIKOLAI>,<M3SC>*/
    /*****************************************************
    *variable declaration
    *****************************************************/
    int i;
    double det, pow_2=1.0, zero = 0, one = 1, two = 2, three = 3,four = 4, nine =9,eleven = 11;
    double alpha, beta, p;
    double benchmark = 4.0e-14;
    double a2, a1, a0;
    double temp_coefs[3];


    a2 = a[2];
    a1 = a[1];
    a0 = a[0];

    /****************************************************
    *Special cases
    *****************************************************/
    //a2 = 0, a1 = 0 and a0 = 0
    if (a2 == 0 && a1==0 && a0==0){
        r[1] = 0;
        r[2] = 0;
        r[3] = 0;
        return 1;
    }
    //a1 = 0 and a0 = 0
    if (a1==0 && a0==0){
        r[2] = 0;
        r[3] = 0;
        temp_coefs[0]=a2;
        temp_coefs[1]=one;
        i = lin_root(temp_coefs, r);
        if ((r[1])==0){
            return 1;
        }
        root_order(r+1,r+2,r+3);
        return 2;
    }
    /****************************************************
    *Question 3 Start
    *****************************************************/

    //Q3(i)
    if (a2==0 && a1 ==0){
        r[1] = cbrt(-a0);
        r[2] = cos(two*pi/three) * (r[1]);
        r[3] = sin(two*pi/three) * ABS_((r[1]));
        return 0;
    }
    // Q3(ii )a0 = 0
    if(a0==0){
        r[1] = 0;
        temp_coefs[0]=a1;
        temp_coefs[1]=a2;
        temp_coefs[2]=one;
        i = quad_root(a,r+1); // as a2 is != 0, a quadratic
        if (i == 0){
            return 0;
        }
        else if ((i == 1) && (r[2] == 0)){
            return 1;
        }
        else if (i == 1){
            root_order(r+1,r+2,r+3);
            return 2;
        }
        else{
            if((r[2])==0 || (r[3])==0){
                root_order(r+1,r+2,r+3);
                return 2;
            }
            root_order(r+1,r+2,r+3);
            return 3;
        }

    }
    //Q3 (iii) a0 = a1*a2
    if (a0==a1*a2){
        if(a1>0){
            r[1] = -a2;
            r[2] = 0;
            r[3] = sqrt(a1);
            return 0;
        }
        r[1] = sqrt(-a1);
        r[2] = -sqrt(-a1);
        r[3] = -a2;
        root_order(r+1,r+2,r+3);
        return 3;
    }
    //Q3 (iv) 3a1 = a2^2 and 27a0 = a2^2
    if((3*a1 == a2*a2) && (27*a0 == a2*a2)){
        r[1] = -a2/3;
        r[2] = -a2/3;
        r[3] = -a2/3;
        return 3;
    }


    /****************************************************
    *Question 3 End (except for (v))
    *****************************************************/

    /****************************************************
    *cubic case
    *****************************************************/
    alpha = getA(a2,a1,a0);
    beta = -a2/three;
    if (alpha == 0){
        //printf("success\n");
        r[1]=-a2/three;
        if((a2*a2/three-a1)<0){
            r[2] = -a2/three;
            r[3] = sqrt(-a2*a2/three+a1);
            return 0;
        }
        r[2]=-a2/three+sqrt(a2*a2/three-a1);
        r[3]=-a2/three-sqrt(a2*a2/three-a1);

        if( ((r[1])==(r[2])) || ((r[2])==(r[3])) || ((r[1])==(r[3])) ){
            if((r[1]) == (r[3]) && ((r[2])==(r[3])) ){
                return 1;
            }
            root_order(r+1,r+2,r+3);
            return 2;
        }
        root_order(r+1,r+2,r+3);
        return 3;
    }

    p = getP(a2,a1,alpha,beta);

    if (p==0){
        //printf("success1\n");
        r[1] = alpha+beta;
        r[2] = cos(two*pi/three)*alpha + beta;
        r[3] = sin(two*pi/three)*alpha;
        return 0;
    }

    /******************************
    *Q3 (v) Start
    ******************************/
    if(ABS_(p-3*cbrt(4))<benchmark){
        r[1] = -alpha*cbrt(0.5)+beta;
        r[2] = -alpha*cbrt(0.5)+beta;
        r[3] =  alpha*cbrt(4)+beta;
        root_order(r+1,r+2,r+3);
        return 2;
    }
    /******************************
    *Q3 (v) End
    ******************************/
    //printf ("a = %lf\n", alpha);
    //printf ("b = %lf\n", beta);
    //printf ("p = %lf\n", p);

    if(p>eleven/three){
        r[1] = newton(sqrt(p),p,benchmark);
    }
    else if(p>-1.92){
        r[1] = newton(1+p/three-p*p*p/nine/nine,p,benchmark);
    }
    else{
        r[1] = newton(-1/p,p,benchmark);
    }

    //printf ("After Newton = %lf\n", (r[1])*(r[1])*(r[1])-p*(r[1])-1   );


    r[1] = alpha*(r[1])+beta;
    temp_coefs[0]=-a0/(r[1]);
    temp_coefs[1]=(a2+(r[1]));
    temp_coefs[2]=one;
    i = quad_root(temp_coefs, r+1);

    //printf("CASE %d  %lf \n", i, r[1]);

    if (i==0){
        return 0;
    }
    else if ((i == 1) && ((r[2]) == (r[1])) ){
        return 1;
    }
    else if (i == 1){
        root_order(r+1,r+2,r+3);
        return 2;
    }
    else{
        if((r[2])==(r[1]) || (r[3])==(r[1])){
            root_order(r+1,r+2,r+3);
            return 2;
        }
        root_order(r+1,r+2,r+3);
        return 3;
    }

}
