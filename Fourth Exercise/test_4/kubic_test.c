/************************************************************************
Nikolai Rozanov,
Feb 2016,
00831231,
M3SC
************************************************************************/
#include <stdio.h>
#include <math.h>
#include <complex.h>

/************************************************************************
Function Declarations
************************************************************************/
#define pi (acos(-1))
#define max(x,y) (((x)>(y)) ? (x) : (y))
#define ABS_(x) (  ((x)>=0) ? (x) : (-(x))  )
#define sgn(x) ( ((x)>=0) ? (1) : (-1)  )


int lin_root(double,double,double *);
int quad_root(double, double, double,double *, double *);
int rcubic_roots(double , double , double , double *, double *, double *);



/************************************************************************
Helper Functions
************************************************************************/

double evaluator(double a2, double a1, double a0, double root){
    return (root*root*root + a2*root*root + a1*root + a0);
}
double complex complex_evaluator(double a2, double a1, double a0, double real_, double imag_){
    double complex root = real_ + imag_*I;
    return (root*root*root + a2*root*root + a1*root + a0);
}



/************************************************************************
Main Function
************************************************************************/
int main(void){


    int t=0,i=0,j=0,k, done=0x0, return_, counter1=0, counter2=0, counter3=0, counter4=0, counter5=0;    //doubles
    double a2_,a1_,a0_, r1_dbl, r2_dbl, r3_dbl,r1, r2, r3;

    double complex result;

    int case_ = 0;
    double det=0, two = 0;
    double benchmark = 1e-8;
    /*********************************************************
    //Main Part of the Program
    *********************************************************/

    //printf("%16s\t%16s\t%16s\t%16s\t%16s\t%16s\n",array_fields[0],array_fields[1],array_fields[2],array_fields[3],array_fields[4],array_fields[5]);



    while (done==0){
        scanf("%lf %lf %lf %d %lf %lf %lf %d", &a2_,&a1_,&a0_,&done,&r1,&r2,&r3, &return_);
        printf("START\n");
        t++;
        k = rcubic_roots(a2_,a1_,a0_,&r1_dbl,&r2_dbl, &r3_dbl);
        printf("what the fuck is going on? %d\n", k);

        if (r3>=0){


            if (ABS_(r1_dbl-r1)>benchmark){
                counter1++;
                (case_) += 10;
            }

            if (ABS_(r2_dbl-r2)>benchmark){
                counter2++;
                (case_) += 200;

            }
            if (ABS_(r3_dbl-r3)>benchmark){
                counter3++;
                (case_) += 3000;
            }
            if(k!=return_){
                counter4++;
                (case_) += 40000;
            }
            i++;
        }
        if ((case_) >= 10){
            printf("CASE:%d   %d  %10.5e %10.5e %10.5e %3.16lf %3.16lf %3.16lf %d\n",t,(case_),a2_,a1_,a0_,r1,r2,r3, return_);
            printf("ROOT Diffs:%3.16lf   %3.16lf %3.16lf return: %d his return: %d\n",r1_dbl-r1,r2_dbl-r2, r3_dbl-r3, k, return_);
            printf("ROOTS:%3.16lf   %3.16lf %3.16lf return: %d his return: %d\n",r1_dbl,r2_dbl,r3_dbl, k, return_);
            if(k!=0){
                printf("MY evaluation: %lf    %lf    %lf\n",evaluator(a2_,a1_,a0_,r1_dbl),evaluator(a2_,a1_,a0_,r2_dbl),evaluator(a2_,a1_,a0_,r3_dbl)  );
                printf("Test evaluation: %lf    %lf    %lf\n",evaluator(a2_,a1_,a0_,r1),evaluator(a2_,a1_,a0_,r2),evaluator(a2_,a1_,a0_,r3)  );
            }
            else{
                printf("My evaluation %lf", evaluator(a2_,a1_,a0_,r1_dbl) );
                result = complex_evaluator(a2_,a1_,a0_,r2_dbl,r3_dbl);
                printf("\t %lf + %lfi\n", creal(result), cimag(result) );

                printf("Test evaluation %lf", evaluator(a2_,a1_,a0_,r1) );
                result = complex_evaluator(a2_,a1_,a0_,r2,r3);
                printf("\t %lf + %lfi\n", creal(result), cimag(result) );
            }
        }
        printf("end\n\n\n");
        (case_)=0;


    }
    printf("\nBenchmark value: %1.3e\n", benchmark);
    printf("Cubic cases failed: r1: %d  r2: %d  r3: %d! Passing precentage: %f\n", counter1, counter2, counter3,((float)(3*i-counter1-counter2-counter3))/((float)(3*i)));
    printf("Cubic Return: %d out of: %d passed! Passing precentage: %f \n",i-counter4,i,((float)(i-counter4))/((float)i));

    //printf("Linear cases failed: %d ! Passing precentage: %f\n", counter5,((float)(j-counter5))/((float)(j)));
    //printf("Linear Return: %d out of: %d passed! Passing precentage: %f \n",j-counter4,j,((float)(j-counter4))/((float)j));


    //i += j;
    //counter3 += counter4;
    //printf("TOTAL Return: %d out of: %d passed! Passing precentage: %f\n\n",i-counter3,i,((float)(i-counter3))/((float)i) );

    return 0;
}




//
