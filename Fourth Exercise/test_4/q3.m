x = linspace(0,2*pi,100);
b = 0.4;
theta = [1.207041 -0.508718 -0.862981 -2.976934];
X = ones(1,4)*3/8;
Y = ones(1,4)*1/2;

x_vec = [cos(theta)' X']
y_vec = [b*sin(theta)' Y']

dx = x_vec(:,2)-x_vec(:,1);  
dy = y_vec(:,2)-y_vec(:,1);

d1 = dy./dx;
d2 = sin(theta)'./cos(theta)'/b;

d1-d2

ellips_x = cos(x);
ellips_y = b*sin(x);

figure()
hold on;
plot(ellips_x, ellips_y, 'r')
hold on;
plot(X,Y,'o')
for i = 1:4
    hold on;
    plot(x_vec(i,:), y_vec(i,:), '-')
end 
