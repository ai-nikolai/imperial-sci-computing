/************************************************************************
Nikolai Rozanov,
Feb 2016,
00831231,
M3SC
************************************************************************/
#include <stdio.h>
#include <math.h>


int lin_root(double a1, double a0, double * r){
    /*<ROZANOV>,<NIKOLAI>,<M3SC>*/
    //special cases
    if(a1==0){
        if(a0!=0){
            return -1;
        }
        return 0;
    }

    //standard cases
    *r = -a0/a1;
    return 1;
}
