function ret = order(roots_)
    


    temp = roots_';
       
  
    ret = zeros(1,4);
    
    imagin = imag(temp);
    realin = real(temp);
    for i = 1:3
        
        if (imagin(i) == 0)
            ret(1) = real(i);
        end
        
        if (imagin(i) > 0)
            ret(2) = real(i);
            ret(3) = imagin(i);
            ret(4) = 0;
        end
    end
    
    if (ret(3)== 0)
        ret(1) = max(realin);
        ret(2) = median(realin);
        ret(3) = min(realin);  
        
        if (ret(1)==ret(2) && ret(3)==ret(1))
            ret(4)=1;
        elseif(ret(1)==ret(2) || ret(3)==ret(2))
            ret(4)=2;
        else
            ret(4)=3;
    end
    
    
end

