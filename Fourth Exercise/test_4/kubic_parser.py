file = open("./cubic_raw.txt", 'r')

case = False
temp = [1]
results = []

counter = 0

lines = file.readlines()


def cases_(line):
    for x in enumerate(line):
        line = line.replace(x[1],'',1)
        if x[1]=='+':
            break


    for x in enumerate(line):
        if (x[1].isdigit()!=True and x[1]!='-' and x[1]!='e' and x[1]!='+' and x[1]!='^' and x[1]!='=' and x[1]!='.' and x[1]!='\\'and x[1]!='n'):
            line = line.replace(x[1],'',1)


    z = list(line)
    for x in enumerate(z):
        if x[1]=="e":
            if z[x[0]+1]=='+':
                z[x[0]+1]='*'
            else:
                z[x[0]+1]='@'
    line = ''.join(z)

    line = line.replace('+',' ')
    line = line.replace('*','+')
    line = line.replace('@','-')

    z = list(line)
    z[-1]='0'
    z[-2]=' '
    for x in enumerate(z):
        if x[1]=="^":
            z[x[0]]=''
            z[x[0]+1]=''
            break

    line = ''.join(z)

    return line

def roots_(line):
    r0 = ' '
    r1 = ' '
    r2 = ' '
    r3 = ' '


    for x in enumerate(line):
        line = line.replace(x[1],'',1)
        if x[1]=='=':
            break

    for x in enumerate(line):
        if (x[1].isdigit() or x[1]=='-' or x[1]=='e' or x[1]=='+' or x[1]=='.'):
                r0 += x[1]
        if(x[1]==','):
            break




    for x in enumerate(line):
        line = line.replace(x[1],'',1)
        if x[1]=='=':
            break

    for x in enumerate(line):
        if (x[1].isdigit() or x[1]=='-' or x[1]=='e' or x[1]=='+' or x[1]=='.'):
                r1 += x[1]
        if(x[1]==','):
            break



    for x in enumerate(line):
        line = line.replace(x[1],'',1)
        if x[1]=='=':
            break
    for x in enumerate(line):
        if (x[1].isdigit() or x[1]=='-' or x[1]=='e' or x[1]=='.' or x[1]=='+'):
                r2 += x[1]
        if(x[1]=='r'):
            break




    for x in enumerate(line):
        line = line.replace(x[1],'',1)
        if x[1]=='=':
            break

    for x in enumerate(line):
        if (x[1].isdigit() or x[1]=='-' or x[1]=='e' or x[1]=='.' or x[1]=='+'):
                r3 += x[1]
        if(x[1]==','):
            break

    return (r0+r1+r2+r3)



for line in lines:

    if (counter == 0):
        line = line.replace(' ','')
        line = line.replace('x','')
        line = cases_(line)
        temp[0] = line

    if (counter == 1):
        line = roots_(line)
        temp[0] += line

    if (counter == 2):
        results.append(temp[0])

    counter = (counter + 1)%3



file.close()


for i in results:
    print i
