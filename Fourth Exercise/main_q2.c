/************************************************************************
Nikolai Rozanov,
Feb 2016,
00831231,
M3SC
************************************************************************/

#include <stdio.h>
#include <math.h>
#include <complex.h>

/************************************************************************
Function Declarations
************************************************************************/
int lin_root(double *, double *);
int quad_root(double *, double *);
int rcubic_roots(double *, double *);
int rquartic_roots(double *, double *);

/************************************************************************
Helper Functions
************************************************************************/
double evaluator(double a3,double a2, double a1, double a0, double root){
    return (root*root*root*root + a3*root*root*root + a2*root*root + a1*root + a0);
}
double complex complex_evaluator(double a3, double a2, double a1, double a0, double real_, double imag_){
    double complex root = real_ + imag_*I;
    return (root*root*root*root + a3*root*root*root + a2*root*root + a1*root + a0);
}

/************************************************************************
Main Function
************************************************************************/
int main(void){
    int k, not_done=0x1, counter=0;;
    double a[4], r[5];
    double complex result;
    /*********************************************************
    //Section for my Information
    *********************************************************/
    // declaring the strings and i
    int my_int_for_info;
    char fields[5][14] = {"Name","CID","LIBRARY NO","Email Adress","Course CODE"};
    char values[5][33] = {"Rozanov, Nikolai","00831231","0247023908","nikolai.rozanov13@imperial.ac.uk", "M3SC"};

    //printing in automated way
    for(my_int_for_info = 0; my_int_for_info<5; my_int_for_info++){
        printf("%12s: <%s>\n",fields[my_int_for_info], values[my_int_for_info]);
    }
    printf("%12s: <%s>\n","Date:", __DATE__);
    printf("%12s: <%s>\n","Time:", __TIME__);

    printf("\n\n");
    /*********************************************************
    //Main Part of the Program
    *********************************************************/
    // getting the Coefficients
    //printf("Enter the coefficients of the Quadratic Equation a2*x^2+a1*x+a0=0 \n");
    //printf("In the order a2,a1,a0, separated by spaces\n");
    while(not_done){
        counter++;
        scanf("%d %lf %lf %lf %lf",&not_done,&a[3],&a[2],&a[1],&a[0]);
        printf("\n\nCase:%d a[3]=%lf a[2]=%lf a[1] = %lf a[0]=%lf\n", counter, a[3],a[2],a[1],a[0]);
        // printing the roots given the type of equation.
        switch((k=rquartic_roots(a,r))){
            case 4:
                printf("Four distinct real roots: r1 = %lf r2 = %lf, r3 = %lf r4 = %lf\n", r[1], r[2] ,r[3], r[4]);
                printf("This is how these roots perform: %lf %lf %lf %lf\n", evaluator(a[3],a[2],a[1],a[0],r[1]),evaluator(a[3],a[2],a[1],a[0],r[2]),evaluator(a[3],a[2],a[1],a[0],r[3]), evaluator(a[3],a[2],a[1],a[0],r[4]));
                break;
            case 3:
                printf("Return is 3! r1 = %lf r2 = %lf, r3 = %lf r4 = %lf\n", r[1], r[2] ,r[3], r[4]);
                printf("This is how these roots perform: %lf %lf %lf %lf\n", evaluator(a[3],a[2],a[1],a[0],r[1]),evaluator(a[3],a[2],a[1],a[0],r[2]),evaluator(a[3],a[2],a[1],a[0],r[3]), evaluator(a[3],a[2],a[1],a[0],r[4]));
                break;
            case 2:
                printf("Two real roots and one complex conjugate pair! r1 = %lf r2 = %lf,  r_real = %lf r_imag = %lf\n", r[1],r[2], r[3], r[4]);
                printf("This is how the real roots perform: %lf %lf\n", evaluator(a[3],a[2],a[1],a[0],r[1]),evaluator(a[3],a[2],a[1],a[0],r[2]) );
                result = complex_evaluator(a[3],a[2],a[1],a[0],r[3],r[4]);
                printf("This is how the complex conjugate pair performs: %lf + %lfi\n", creal(result), cimag(result) );
                break;
            case 1:
                printf("Return is 1! r1 = %lf r2 = %lf, r3 = %lf r4 = %lf\n", r[1], r[2] ,r[3], r[4]);
                printf("This is how these roots perform: %lf %lf %lf %lf\n", evaluator(a[3],a[2],a[1],a[0],r[1]),evaluator(a[3],a[2],a[1],a[0],r[2]),evaluator(a[3],a[2],a[1],a[0],r[3]), evaluator(a[3],a[2],a[1],a[0],r[4]));
                break;
            case 0:
                printf("Two complex conjugate pairs: r_real = %lf r_imag = %lf, r_real = %lf r_imag = %lf\n", r[1], r[2] ,r[3], r[4]);
                result = complex_evaluator(a[3],a[2],a[1],a[0],r[1],r[2]);
                printf("This is how the first complex conjugate pair performs: %lf + %lfi\n", creal(result), cimag(result) );
                result = complex_evaluator(a[3],a[2],a[1],a[0],r[3],r[4]);
                printf("This is how the second complex conjugate pair performs: %lf + %lfi\n", creal(result), cimag(result) );
                break;
            default:
                printf("Error in the code, this case does not exist! \n");
        }
        printf("End of Case:%d\n", counter);

    }

    return 0;
}
