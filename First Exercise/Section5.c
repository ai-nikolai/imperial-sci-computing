#include <stdio.h>

int main(void){
    /*********************************************************
    //variable declaration
    *********************************************************/
    int temp1 = (127-24)<<23, temp2 = (127-53)<<23, temp3 = (127-113)<<23;
    float frac1, frac2, frac3;

    /*********************************************************
    //additional part of the section done in C
    *********************************************************/
    float x1=1.0e6,x2=2.4048255576957728,x3;

    //masking the exponent of the float representation
    *((int*) &frac1) = temp1;
    *((int*) &frac2) = temp2;
    *((int*) &frac3) = temp3;
    //printing the result
    printf("2^-24 = %1.3e\n",frac1);
    printf("2^-53 = %1.3e\n",frac2);
    printf("2^-113 = %1.3e\n\n",frac3);


    /*********************************************************
    **********************************************************
    //the main part of the section
    **********************************************************
    *********************************************************/
    x3=x1+x2;
    printf("x1, x2, x3 = %f %f %f \n", x1,x2,x3);
    //a
    x3=x1*x2;
    printf("x1, x2, x3 = %f %f %f \n",x1,x2,x3);
    //b
    x3=x1/x2;
    printf("x1, x2, x3 = %f %f %f \n",x1,x2,x3);
    //c
    x3=x2/x1;
    printf("x1, x2, x3 = %f %f %f \n", x1,x2,x3);
    //d generates warning
    //x3=x1%x2;
    //printf("x1, x2, x3 = %f %f %f \n",x1,x2,x3);

    //e
    x1=6.02214e23;
    x2=3.e17;

    x3=x1+x2;
    printf("x1, x2, x3 = %f %f %f \n", x1,x2,x3);
    //f
    x3=x1*x2;
    printf("x1, x2, x3 = %f %f %f \n",x1,x2,x3);
    //g
    x3=x1/x2;
    printf("x1, x2, x3 = %f %f %f \n",x1,x2,x3);
    //h
    x3=x2/x1;
    printf("x1, x2, x3 = %f %f %f \n", x1,x2,x3);
    //i generates warning
    //x3=x1%x2;
    //printf("x1, x2, x3 = %f %f %f \n",x1,x2,x3);


    return 0;
}
