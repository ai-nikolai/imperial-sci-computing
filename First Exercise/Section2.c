#include <stdio.h>

int main(void){
    /*********************************************************
    //variable declaration
    *********************************************************/
    char sign_array[11] = {'\n','\t','\v','\b','\r','\f','\a','\\','\?','\'','\"'};
    char char_array[11] = {'n','t','v','b','r','f','a','\\','\?','\'','\"'};




    /*********************************************************
    //Section for my Information
    *********************************************************/

    // declaring the strings and i
    int i;
    char fields[5][14] = {"Name","CID","LIBRARY NO","Email Adress","Course CODE"};
    char values[5][33] = {"Rozanov, Nikolai","00831231","0247023908","nikolai.rozanov13@imperial.ac.uk", "M3SC"};

    //printing in automated way
    for(i = 0; i<5; i++){
        printf("%12s: <%s>\n",fields[i], values[i]);
    }
    printf("\n\n");





    /*********************************************************
    **********************************************************
    //the main part of the section
    **********************************************************
    *********************************************************/
    for(i = 0; i<11; i++){
        printf("Hello%cWorld!\t// with \\%c \t\n", sign_array[i], char_array[i]);
    }
    return 0;

}
