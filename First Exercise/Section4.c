#include <stdio.h>

int main(void){

    short i1 = 44, i2 = 55, i3;

    i3 = i1+i2;
    printf("i1, i2, i3 = %d %d %d \n", i1,i2,i3);
    //a
    i3 = i1*i2;
    printf("i1, i2, i3 = %d %d %d \n", i1,i2,i3);
    //b
    i3 = i1/i2;
    printf("i1, i2, i3 = %d %d %d \n", i1,i2,i3);
    //c
    i3 = i2/i1;
    printf("i1, i2, i3 = %d %d %d \n", i1,i2,i3);


    //d
    i1 = 44444;
    i2 = 55555;
    i3 = i1+i2;
    printf("i1, i2, i3 = %d %d %d \n", i1,i2,i3);
    //e
    i3 = i1*i2;
    printf("i1, i2, i3 = %d %d %d \n", i1,i2,i3);
    //f
    i3 = i1/i2;
    printf("i1, i2, i3 = %d %d %d \n", i1,i2,i3);
    //g
    i3 = i2/i1;
    printf("i1, i2, i3 = %d %d %d \n", i1,i2,i3);



    // rest of section done in C
    printf("99999-2^16 = %d\n\n", 99999-0x10000);


    return(0);
}
