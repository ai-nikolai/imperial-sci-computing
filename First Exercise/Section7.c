#include <stdio.h>

int main(void){
    double a1,a0;

    printf("enter coefficients of Linear Equation a1*x+a0=0 \n");
    printf("in the order a1,a0, separated by spaces\n");
    scanf("%lf %lf",&a1,&a0);

    if(a1==0){
        if(a0!=0){
            printf("This equation has no solution! With a1 = %lf, a0 = %lf, a0/a1 = %lf \n", a1,a0,a0/a1);
            return 0;
        }
        printf("This equation is always true! With a1 = %lf, a0 = %lf, a0/a1 = %lf \n", a1,a0,a0/a1);
        return 0;
    }

    printf("The equation has a solution! With a1 = %lf, a0 = %lf, x=-a0/a1 = %lf \n", a1,a0,-a0/a1);
    return 0;

    return 0;

}
