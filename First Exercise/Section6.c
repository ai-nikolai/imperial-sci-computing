#include <stdio.h>


int main(void){
    //variable declaration
    int high=160,step=5,C=-40;
    float F;
    char names[4][11] = {"Kelvin","Celsius","Fahrenheit","Rankine"};

    // description of numbers
    printf("%10s %10s %12s %10s \n", names[1],names[0],names[2],names[3]);

    //main part, with conversion into different Temperature measures.
    while(C <= high){
        F=32.0+9.0*C/5.0;//*((float)C);
        printf("%10d %10.2f   %10.1f %10.2f \n",C,C+273.15,F,F+459.67);
        C=C+step;
    }

    return 0;
}
