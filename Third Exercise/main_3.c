/************************************************************************
Nikolai Rozanov,
Feb 2016,
00831231,
M3SC
************************************************************************/

#include <stdio.h>
#include <math.h>

/************************************************************************
Function Declarations
************************************************************************/
int lin_root(double,double,double *);
int quad_root(double, double, double,double *, double *);
int rcubic_roots(double , double , double , double *, double *, double *);



/************************************************************************
Main Function
************************************************************************/
int main(void){
    int i = 1, j = 0, k;
    double a2,a1,a0, r1,r2, r3;
    double alpha = 0.2135, beta = 0.01709, R = 0.0820578;
    double T[3]={40.0,45.0,50.0};
    double P = 1.0, dP = 0.5;
    double vec[3];
    /*********************************************************
    //Section for my Information
    *********************************************************/
    //declaring the strings and i
    int my_int_for_info;
    char fields[5][14] = {"Name","CID","LIBRARY NO","Email Adress","Course CODE"};
    char values[5][33] = {"Rozanov, Nikolai","00831231","0247023908","nikolai.rozanov13@imperial.ac.uk", "M3SC"};

    //printing in automated way
    for(my_int_for_info = 0; my_int_for_info<5; my_int_for_info++){
        printf("%12s: <%s>\n",fields[my_int_for_info], values[my_int_for_info]);
    }
    printf("\n\n");
    /*********************************************************
    //Main Part of the Program
    *********************************************************/
    for(P=1.0;P<=30.0;P+=dP){
        a1 = alpha/P;
        a0 = beta*alpha/P;
        for(j=0;j<3;j++){
            a2 = beta - T[j];
            k = rcubic_roots(a2,a1,a0,&r1,&r2,&r3);
            if (k==1){
                vec[0] = P;
                vec[1] = T[j];
                vec[2] = r1;
            }
            printf("%lf ", r1);
        }
        printf("%lf \n", P);
    }
    //printf("Values P, T, V when three solutions for V are the same: P: %lf, T: %lf, V:%lf\n", vec[0],vec[1],vec[2]);
    return 0;
}
