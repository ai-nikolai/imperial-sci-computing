/************************************************************************
Nikolai Rozanov,
Feb 2016,
00831231,
M3SC
************************************************************************/

#include <stdio.h>
#include <math.h>

/************************************************************************
Function Declarations
************************************************************************/
int lin_root(double,double,double *);
int quad_root(double, double, double,double *, double *);
int rcubic_roots(double , double , double , double *, double *, double *);



/************************************************************************
Main Function
************************************************************************/
int main(void){
    double a2,a1,a0, r1,r2, r3;
    /*********************************************************
    //Section for my Information
    *********************************************************/
    // declaring the strings and i
    int my_int_for_info;
    char fields[5][14] = {"Name","CID","LIBRARY NO","Email Adress","Course CODE"};
    char values[5][33] = {"Rozanov, Nikolai","00831231","0247023908","nikolai.rozanov13@imperial.ac.uk", "M3SC"};

    //printing in automated way
    for(my_int_for_info = 0; my_int_for_info<5; my_int_for_info++){
        printf("%12s: <%s>\n",fields[my_int_for_info], values[my_int_for_info]);
    }
    printf("\n\n");
    /*********************************************************
    //Main Part of the Program
    *********************************************************/
    // getting the Coefficients
    printf("Enter the coefficients of the Quadratic Equation a2*x^2+a1*x+a0=0 \n");
    printf("In the order a2,a1,a0, separated by spaces\n");
    scanf("%lf %lf %lf",&a2,&a1,&a0);
    // printing the roots given the type of equation.
    switch(rcubic_roots(a2,a1,a0,&r1,&r2, &r3)){
        case 3:
            printf("Three distinct real roots: r1 = %lf, r2 = %lf, r3 = %lf\n",r1, r2, r3);
            break;
        case 2:
            printf("One real root with double multiplicity, one with single: r1 = %lf r2 = %lf\n", r1,r2);
            break;
        case 1:
            printf("One real root with triple multiplicity: r1 = %lf \n", r1);
                break;
        case 0:
            printf("One real root and complex conjugate pair: r1 = %lf, r_real = %lf r_imag = %lf\n", r1, r2 ,r3);
            break;
        case -1:
            printf("Two real root (quadratic equation): r1 = %lf r2 = %lf\n", r1,r2);
            break;
        case -2:
            printf("One real root with double multiplicity (quadratic equation): r1 = %lf\n", r1);
            break;
        case -3:
            printf("Complex conjugate case (quadratic equation) real: %lf  imag: %lf\n", r1,r2);
            break;
        case -4:
            printf("One solution (linear case): r1 = %lf\n", r1);
            break;
        case -5:
            printf("Every number satisfies the equation(0=0)\n");
            break;
        case -6:
            printf("False statement (a0=0)\n");
        default:
            printf("Error in the code, this case does not exist! \n");
    }
    return 0;
}
