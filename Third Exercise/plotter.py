"""
Nikolai Rozanov
December 2015
00831231
M3SC

Python script to plot
"""

import numpy as np
import matplotlib.pyplot as plt

#loading f
dat=np.array(np.loadtxt('result.txt',delimiter=' '))

y1 = np.transpose(dat)[0][:]
y2 = np.transpose(dat)[1][:]
y3 = np.transpose(dat)[2][:]
x = np.transpose(dat)[3][:]

plt.figure("Gas Equation")
plt.plot(x, y1, color = (0.5,0.1,0.11), ls='.',marker = 'o')
plt.plot(x, y2, color = (0.1,0.5,0.1), ls='.',marker = 'o')
plt.plot(x, y3, color = (0.1,0.1,0.5), ls='.',marker = 'o')
plt.axis('tight')
plt.xlabel('P...Pressure')
plt.ylabel('V...Volume')
plt.grid()
plt.title('Nikolai Rozanov, Gas Equation')
plt.show()
