
/************************************************************************
Nikolai Rozanov,
Feb 2016,
00831231,
M3SC
************************************************************************/

#include <stdio.h>
#include <math.h>


/************************************************************************
Function Declarations
************************************************************************/
int lin_root(double,double,double *);
int quad_root(double, double, double,double *, double *);
int rcubic_roots(double , double , double , double *, double *, double *);



/************************************************************************
Helper Functions
************************************************************************/
double evaluate(double a2, double a1, double a0, double root){
    return (1*root*root*root + a2*root*root + a1*root +a0);
}

/************************************************************************
Main Function
************************************************************************/
int main(void){


    int k, done=0x0;
    double a2,a1,a0, r1, r2, r3;


    /*********************************************************
    //Main Part of the Program
    *********************************************************/

    //printf("%16s\t%16s\t%16s\t%16s\t%16s\t%16s\n",array_fields[0],array_fields[1],array_fields[2],array_fields[3],array_fields[4],array_fields[5]);


    while (done==0){
        scanf("%lf %lf %lf %d", &a2,&a1,&a0,&done);

        k = rcubic_roots(a2,a1,a0,&r1,&r2, &r3);
        switch(k){
            case 3:
                printf("%lf %lf %lf %d\n",evaluate(a2,a1,a0,r1), evaluate(a2,a1,a0,r2), evaluate(a2,a1,a0,r3),k);
                break;
            case 2:
                printf("%lf %lf %lf %d\n",evaluate(a2,a1,a0,r1), evaluate(a2,a1,a0,r2), evaluate(a2,a1,a0,r3),k);
                break;
            case 1:
                printf("%lf %d\n",evaluate(a2,a1,a0,r1),k);
                break;
            case 0:
                printf("%d\n",k);
                break;
            case -1:
                printf("%lf %lf %d\n",evaluate(a2,a1,a0,r1), evaluate(a2,a1,a0,r2),k);
                break;
            case -2:
                printf("%lf %d\n",evaluate(a2,a1,a0,r1),k);
                break;
            case -3:
                printf("%d\n",k);
                break;
            case -4:
                printf("%lf %d\n",evaluate(a2,a1,a0,r1),k);
                break;
            case -5:
                printf("Every number satisfies the equation(0=0)\n");
            case -6:
                printf("False statement (a0=0)\n");
            default:
                printf("Error in the code, this case does not exist! \n");
        }




    }

    return 0;
}




//
