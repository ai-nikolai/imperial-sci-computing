/************************************************************************
Nikolai Rozanov,
Feb 2016,
00831231,
M3SC
************************************************************************/
#include <stdio.h>
#include <math.h>
#include <complex.h>

#define max(x,y) (((x)>(y)) ? (x) : (y))
#define ABS_(x) (  ((x)>=0) ? (x) : (-(x))  )

/************************************************************************
Helper Functions
************************************************************************/
void printer(double a, double b, const char * text){
    printf("%s %.16lf %lx  %.16lf %lx\n", text, a, *((long int *) &a),  b, *((long int *) &b)  );
}

double evaluator(double * a, double r){
    return(a[2]*r*r + a[1]*r + a[0]);
}


void comparer(double test, double copy){

    long int mask, mask2, mask3, temp, temp2, temp3;

    mask = *((long int *) &test);
    mask2 = (mask>>52) & 03777;
    mask3 = (mask<<(64-52))>>(64-52);

    temp = *((long int *) &copy);
    temp2 = (temp>>52) & 03777;
    temp3 = (temp<<(64-52))>>(64-52);


    printf("\nHexadecimal Complete:%li %li\n",mask, temp);
    printf("Hexadecimal Complete:%li\n",mask-temp);
    printf("realvalue: %li %li\n", mask2, temp2);
    printf("Hexadecimal Complete:%li\n",mask2-temp2);
    printf("realvalue: %li %li\n", mask3, temp3);
    printf("Hexadecimal Complete:%li\n",mask3-temp3);
}


long int long_min(long int x, long int y){
    return ((x<=y) ? x : y);
}

long int long_max(long int x, long int y){
    return ((x>=y) ? x : y);
}

//to use give pow_2 a long int, it will create a power of 2, accoridng to IEEE standards, ie 2^i, i[-1023,1024]
double pow_2_(long int exp){
    double divisor;
    *((long int *) &divisor) =  exp<<52;
    return divisor;
}

/*
 *
 *
 * QUAD_ROOT (ASSESSMENT)
 *
 *
 */

 int lin_root(double * a, double * r){
     /*<ROZANOV>,<NIKOLAI>,<M3SC>*/
     //special cases
     if(a[1]==0){
         if(a[0]!=0){
             return -1;
         }
         return 0;
     }
     //standard cases
     r[1] = -a[0]/a[1];
     return 1;
 }


  int quad_root(double * a, double * r){
      /*<ROZANOV>,<NIKOLAI>,<M3SC>*/
      /*****************************************************
       *variable declaration
       *****************************************************/
      int i;
      double det, pow_2=1.0;
      long int my_exponent=0, a2_mask, a1_mask, a0_mask, min_mask, max_mask;
      double a2,a1,a0;
      double temp_coefficients[2];

      a2 = a[2];
      a1 = a[1];
      a0 = a[0];

      /****************************************************
       *the linear case:
       *****************************************************/

      if (a2==0){
          printf("case 1 %lf %lf\n",a0,a1);
          temp_coefficients[0]=a0;
          temp_coefficients[1]=a1;
          i = lin_root(temp_coefficients,r);
          return i-2;
      }

      // a0 = 0 case:
      if(a0==0){
          printf("case 2 %lf %lf\n",a0,a1);
          temp_coefficients[0]=a1;
          temp_coefficients[1]=a2;
          i = lin_root(temp_coefficients,r+1); // as a2 is != 0, we have that the linear equation always has one root

          //returning the value and roots the right way!
          if(r[2]<0){
              r[1] = 0;
              return 2;
          }
          if (r[2]==0){
              r[1] = 0;
              return 1;
          }
          if (r[2]>0){
              r[1] = r[2];
              r[2] = 0;
              return 2;
          }
      }


      /****************************************************
       *the quadratic case:
       *****************************************************/
      if (a1==0){
          if(-a0/a2<0){
              r[1] = 0;
              r[2] = sqrt(a0/a2);
              return 0;
          }
          else{
              r[2] = -sqrt(-a0/a2);
              r[1] = -(r[2]);
              return 2;
          }
      }



      /***********************
       *Diskriminant
       ************************/

      //determining the exponent(and sign) of the coefficients

      a2_mask = ((*((long int *) &a2)) >> 52);//  //determines the exponent of a double
      a1_mask = ((*((long int *) &a1)) >> 52);//
      a0_mask = ((*((long int *) &a0)) >> 52);//

      /*
       *MASKS to make use of a2_mask, a1_mask, a0_mask
       *
       *04000 to determine sign bit
       *03777 get rid of the sign bit.
       *
       */

      //determining min/max of the exponent of the coefficients
      min_mask    = fmin((a2_mask & 03777),fmin(a1_mask & 03777,a0_mask & 03777)); //minimum exponent of a2,a1,a0
      max_mask    = fmax((a2_mask & 03777),fmax(a1_mask & 03777,a0_mask & 03777)); //maximum exponent of a2,a1,a0



      if( max_mask>(1023+24) || (max_mask-min_mask>=1023) ){
          my_exponent += 2046-max_mask; // determining by how much to divide
          *((long int *) &pow_2) =  my_exponent<<52; //creating the divisor (such that double precision is preserved)

      }
      else if( min_mask<(1023-24) ){
          my_exponent += 2046-min_mask;
          *((long int *) &pow_2) =  my_exponent<<52;//creating the divisor (such that double precision is preserved)
      }



      //
      a2 = a2*pow_2;
      a1 = a1*pow_2;
      a0 = a0*pow_2;
      det = (a1*a1)-((double)4)*(a2*a0) ;

      if(det<0){ //checking for the complex case
          //printf("Case5\n");
          if(((a2_mask & 03777)<(1023-200)) && (my_exponent<(1023-200))){
              r[1] = -a[1]/(((double)2) * a[2]);
              r[2] = sqrt(-det)/(((double)2) * ABS_(a[2]));
              return 0;
          }
          r[1] = -a1/(((double)2) * a2);
          r[2] = sqrt(-det)/(((double)2) * ABS_(a2));
          return 0;
      }

      det = sqrt(det);


      /****************************
       *solving the actual equation
       *****************************/

      //checking for det = 0 case
      if (det==0){
          //printf("Case4\n");
          if(((a2_mask & 03777)<(1023-200)) && (my_exponent<(1023-200))){
              r[2] = -a[1]/(((double)2) * a[2]);
              r[1] = r[2];
              return 1;
          }
          r[2] = -a1/(((double)2) * a2);
          r[1] = r[2];
          return 1;
      }


      //two distinct real roots (floating point version)
      if (a1<0){
          if(((a2_mask & 03777)<(1023-200)) && (my_exponent<(1023-200))){
              r[1] = (-a[1] + det) /  (((double)2) * a[2]) ;
          }
          else{
               r[1] = (-a1 + det) /  (((double)2) * a2) ;
           }
      }
      else{
          if(((a2_mask & 03777)<(1023-200)) && (my_exponent<(1023-200))){
              r[1] = -(a[1] + det) /  (((double)2) * a[2]) ;
          }
          else{
              r[1] = -(a1 + det) /  (((double)2) * a2);
          }
      }


      //getting the other root.

      if (ABS_((r[1]))>pow_2_(1023-26)){
          if(((a2_mask & 03777)<(1023-200)) && (my_exponent<(1023-200))){
              r[2] = (a[0]/a[2])/(r[1]);
          }
          else{
              r[2] = (a0/a2)/(r[1]);
          }
      }

      else if(a1<0){
          r[2] = -(a1 + det) /  (((double)2) * a2);
      }
      else{
          r[2] = (-a1 + det) /  (((double)2) * a2);
      }



      //ordering roots
      if ((r[2])>(r[1])){
          det = (r[1]);
          (r[1]) = (r[2]);
          (r[2]) = det;
      }

      return 2;


  }




/************************************************************************
Main Function
************************************************************************/
int main(void){

    /*******************************************************************
    ********************************************************************
    SOURCE for Radius:
        Charon: https://en.wikipedia.org/wiki/Charon_(moon)
        Pluto: https://en.wikipedia.org/wiki/Pluto
    ********************************************************************
    *******************************************************************/

    int t=0,i=0,j=0,k, done=0x0, r3, counter1=0, counter2=0, counter3=0, counter4=0, counter5=0;    //doubles
    double a2_,a1_,a0_, r1_dbl, r2_dbl, r1, r2;

    int case_ = 0;
    double det=0, two = 0;
    double benchmark = 1e-4;
    double a[3], r[3];
    /*********************************************************
    //Main Part of the Program
    *********************************************************/

    //printf("%16s\t%16s\t%16s\t%16s\t%16s\t%16s\n",array_fields[0],array_fields[1],array_fields[2],array_fields[3],array_fields[4],array_fields[5]);



    while (done==0){
        scanf("%lf %lf %lf %d %lf %lf %d", &a2_,&a1_,&a0_,&done,&r2,&r1,&r3);
        t++;
        a[0] = a0_;
        a[1] = a1_;
        a[2] = a2_;
        k = quad_root(a,r);
        r1_dbl = r[1];
        r2_dbl = r[2];

        if (r3>=0){


            if (ABS_(r1_dbl-r1)>benchmark){
                //printf(" here : %lf  %lf\n",ABS_(r1_dbl-r1), r1_dbl-r1);
                counter1++;
                (case_) += 20;
            }

            if (ABS_(r2_dbl-r2)>benchmark){
                counter2++;
                (case_) += 200;

            }
            if (k!=r3){
                counter3++;
                (case_) += 3000;
            }


            i++;
        }

        if (r3==-1){

            if (ABS_(r1_dbl-r1)>benchmark){
                counter5++;
                (case_) +=40000;
            }

            if (k!=r3){
                counter4++;
                (case_) += 500000;
            }

            j++;
        }

        if ((case_) > 5000000){
            printf("\n\n\nCASE:%d   %d  %10.5e %10.5e %10.5e %3.16lf %3.16lf %d\n",t,(case_),a2_,a1_,a0_,r1,r2,r3);
            printf("ROOT Diffs:%3.16lf   %3.16lf return: %d his return: %d\n",r1_dbl-r1,r2_dbl-r2,k, r3);
            printf("ROOTS:%3.16lf   %3.16lf return: %d his return: %d\n",r1_dbl,r2_dbl,k, r3);
            printf("DET: %3.16lf\n", det);
            printf("A2:%10.5e A1:%3.16lf A0:%10.5e EXP:%10.5e\n",a2_*two,a1_*two,a0_*two,two);
            printf("PERFOMANCE %3.15lf %3.15lf\n",evaluator(a,r[1]), evaluator(a,r[2]));
        }

        //if ((evaluator(a,r1)>benchmark || evaluator(a,r2)>benchmark) && k>0){
        if(k>0 && case_>19){
            printf("\n\n\nCASE:%d   %d  %10.5e %10.5e %10.5e %3.16lf %3.16lf %d\n",t,(case_),a2_,a1_,a0_,r1,r2,r3);
            printf("ROOTS:%3.16lf   %3.16lf return: %d his return: %d\n",r[1],r[2],k, r3);
            printf("PERFOMANCE %3.15lf %3.15lf\n",evaluator(a,r[1]), evaluator(a,r[2]));
        }
        (case_)=0;


    }
    printf("\nBenchmark value: %1.3e\n", benchmark);
    printf("Quadratic cases failed: r1: %d  r2: %d! Passing precentage: %f\n", counter1, counter2,((float)(2*i-counter1-counter2))/((float)(2*i)));
    printf("Quadtratic Return: %d out of: %d passed! Passing precentage: %f \n",i-counter3,i,((float)(i-counter3))/((float)i));

    //printf("Linear cases failed: %d ! Passing precentage: %f\n", counter5,((float)(j-counter5))/((float)(j)));
    //printf("Linear Return: %d out of: %d passed! Passing precentage: %f \n",j-counter4,j,((float)(j-counter4))/((float)j));


    i += j;
    counter3 += counter4;
    printf("TOTAL Return: %d out of: %d passed! Passing precentage: %f\n\n",i-counter3,i,((float)(i-counter3))/((float)i) );

    return 0;
}




//
