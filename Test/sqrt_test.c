#include <stdio.h>
#include <math.h>


double pow_2(long int exp){
    double divisor;
    *((long int *) &divisor) =  exp<<52;
    return divisor;
}



int main(){
    int i;
    double test =1.1248005, test2=2.000023063,sq, real_sq=3.124823563, temp;
    long int mask, mask2;


    mask = *((long int *) &real_sq);

    sq = test+test2;
    mask2 = *((long int *) &sq);

    printf("difference: %li\n", mask-mask2);

    for(i=100;i<140;i++){

        printf("\nCASE: %d\n", i);

        temp = (((test*pow_2(1023+i))+(test2*pow_2(1023+i)))*((test*pow_2(1023+i))+(test2*pow_2(1023+i))));

        sq = sqrt(temp)/pow_2(1023+i);
        mask2 = *((long int *) &sq);

        printf("difference: %li\n", mask-mask2);
        //printf("test %lf\n", sq);

        temp = (((test*pow_2(1023-i))+(test2*pow_2(1023-i)))*((test*pow_2(1023-i))+(test2*pow_2(1023-i))));
        sq = sqrt(temp)/pow_2(1023-i);
        mask2 = *((long int *) &sq);

        printf("difference: %li\n", mask-mask2);
        //printf("test %lf\n", sq);
    }


}
