/************************************************************************
Nikolai Rozanov,
Jan 2016,
00831231,
M3SC
************************************************************************/

#include <stdio.h>
#include <math.h>
#include <complex.h>


#define pi acos(-1);




void comparer(double test, double copy){

    long int mask, mask2, mask3, temp, temp2, temp3;

    mask = *((long int *) &test);
    mask2 = (mask>>52) & 03777;
    mask3 = (mask<<(64-52))>>(64-52);

    temp = *((long int *) &copy);
    temp2 = (temp>>52) & 03777;
    temp3 = (temp<<(64-52))>>(64-52);

    printf("\nHexadecimal Complete:%lx %lx\n",mask, temp);
    printf("\nHexadecimal Complete:%li\n",mask-temp);
    printf("Hexadecimal Complete:%li\n",mask2-temp2);
    printf("Hexadecimal Complete:%li\n",mask3-temp3);
}





double pow_2(long int exp){
    double divisor;
    *((long int *) &divisor) =  exp<<52;
    return divisor;
}

int main(void){

    double test = 1053.23511, copy;
    long int mask, mask2, mask3, temp_mask, temp, temp2, temp3;

    //copy = test;


    copy = test;


    copy*=pow_2(512);
    comparer(test,copy);
    copy/=pow_2(512);

    comparer(test,copy);



    /*
    mask = *((long int *) &test);
    temp_mask = mask;
    mask2 = (mask>>52) & 03777;
    mask3 = (mask<<(64-52))>>(64-52);
    printf("Hexadecimal Complete:%lo \t %li\n", mask, mask);
    printf("Hexadecimal Complete:%lo \t %li\n", mask2, mask2);
    printf("Hexadecimal Complete:%lo \t %li\n", mask3, mask3);



    test = test*pow_2(1023+10);
    mask = *((long int *) &test);
    temp2 = (mask>>52) & 03777;
    temp3 = (mask<<(64-52))>>(64-52);
    printf("Hexadecimal Complete:%lo \t %li\n", mask, mask);
    printf("Hexadecimal Complete:%lo \t %li\n", temp2, temp2);
    printf("DIFFERENCE:%lo \t %li\n", mask2-temp2, mask2-temp2);
    printf("Hexadecimal Complete:%lo \t %li\n", temp3, temp3);
    printf("DIFFERECNCE:%lo \t %li\n", mask3-temp3, mask3-temp3);

    test = test*pow_2(1023-10);
    mask = *((long int *) &test);
    mask2 = (mask>>52) & 03777;
    mask3 = (mask<<(64-52))>>(64-52);
    printf("\nBIIIG TEST:%li  %lf\n\n", mask-temp_mask, test-copy);
    printf("Hexadecimal Complete:%lo \t %li\n", temp2, temp2);
    printf("DIFFERECNCE:%lo \t %li\n", mask2-temp2, mask2-temp2);
    printf("Hexadecimal Complete:%lo \t %li\n", temp3, temp3);
    printf("DIFFERENCE:%lo \t %li\n", mask3-temp3, mask3-temp3);

    */
    /*
    mask = *((long int *) &test);
    printf("Hexadecimal Complete:%lo \t %li\n", mask, mask);

    mask = (*((long int *) &test) >> 52) & 03777;
    printf("Hexadecimal Exponent:%lo \t %li\n", mask, mask);

    copy = test;
    copy /= (double) (1<<20);
    mask = (*((long int *) &copy) >> 52) & 03777;
    printf("Hexadecimal Exponent:%lo \t %li\n", mask, mask);


    temp_mask = 04000;
    temp = ~0;
    temp_mask = (temp_mask<<52) & (temp);
    temp_mask = (*((long int *) &copy)) & (temp<<52);
    printf("test %lo\n", temp_mask);
    *((long int *) &test) = ((((*((long int *) &test) >> 52) & 03777)-20) << 52) & temp_mask;
    mask = (*((long int *) &test) >> 52) & 03777;
    printf("Hexadecimal Exponent:%lo \t %li\n", mask, mask);
    */
    return 0;
}
