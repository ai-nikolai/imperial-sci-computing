#include <stdio.h>

double pow_2(long int exp){
    double divisor;
    *((long int *) &divisor) =  exp<<52;
    return divisor;
}


void quad_generator(double r1, double r2, double * a1, double * a0){
    *a1 = -(r1+r2);
    *a0 = r1*r2;

}

int main(){

    int i;
    double a1_,a0_,a2 = 1.0, a1 = -14.0, a0 = 49.0, delta=0.0006, bias, r1 = 1.0, r2 = 2.0;
    /*
    for (i=0;i<11;i++){
        r1 *= pow_2(1023+i);
        r2 *= pow_2(1023-i);
        quad_generator(r1, r2, &a1_, &a0_);
        printf("%.16lf %.16lf %.16lf %.16lf %.16lf %d\n",1.0, a1_, a0_, (r2>r1)?r2:r1,(r2<r1)?r2:r1,0);
    }
    */

    /*for(i=0;i<25;i++){
        bias = delta*pow_2((long int)(i+1023));
        printf("%lf %lf %lf %d\n",a2*bias,a1*bias,a0*bias,0);
    }
    */
    /*for(i=100;i<125;i++){
        bias = delta*pow_2((long int)(i+1023));
        printf("%lf %lf %lf %d\n",a2*bias,a1*bias,a0*bias,0);
    }
    */
    for(i=12;i<49;i++){
        bias = delta/pow_2((long int)(i+1023));
        printf("%.16lf %.16lf %.16lf %.16lf %.16lf %d\n",a2*bias,a1*bias,a0*bias,7.0,7.0,0);
    }
    bias = delta*pow_2((long int)(650+1023));
    printf("%.16lf %.16lf %.16lf %.16lf %.16lf %d\n",a2*bias,a1*bias,a0*bias,7.0,7.0,0);


    r1 = 1.0;
    r2 = 2.0;
    quad_generator(r1,r2, &a1, &a0);
    printf("%.16lf %.16lf %.16lf %.16lf %.16lf %d\n",1.0, a1, a0, r1,r2,1);

    return 0;

}
