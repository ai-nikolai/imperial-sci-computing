#include <stdio.h>

double pow_2(long int exp){
    double divisor;
    *((long int *) &divisor) =  exp<<52;
    return divisor;
}


int main(){

    int i;
    double a2 = 1.0, a1 = -14.0, a0 = 49.0, delta=0.0006, bias;

    /*for(i=0;i<25;i++){
        bias = delta*pow_2((long int)(i+1023));
        printf("%lf %lf %lf %d\n",a2*bias,a1*bias,a0*bias,0);
    }
    */
    /*for(i=100;i<125;i++){
        bias = delta*pow_2((long int)(i+1023));
        printf("%lf %lf %lf %d\n",a2*bias,a1*bias,a0*bias,0);
    }
    */
    for(i=21;i<50;i++){
        bias = delta/pow_2((long int)(i+1023));
        printf("%.16lf %.16lf %.16lf %d\n",a2*bias,a1*bias,a0*bias,0);
    }
    bias = delta*pow_2((long int)(650+1023));
    printf("%lf %lf %lf %d\n",a2*bias,a1*bias,a0*bias,0);


    printf("%lf %lf %lf %d\n",1.0, -14.0, 49.0, 1);

    return 0;

}
