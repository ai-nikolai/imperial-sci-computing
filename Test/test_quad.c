/************************************************************************
Nikolai Rozanov,
Feb 2016,
00831231,
M3SC
************************************************************************/
#include <stdio.h>
#include <math.h>
#include <complex.h>

#define max(x,y) ((x>y) ? (x) : (y))
#define ABS_(x) ((x>=0) ? (x) : (-x))
/************************************************************************
Helper Functions
************************************************************************/
void printer(double a, double b, const char * text){
    printf("%s %.16lf %lx  %.16lf %lx\n", text, a, *((long int *) &a),  b, *((long int *) &b)  );
}


void comparer(double test, double copy){

    long int mask, mask2, mask3, temp, temp2, temp3;

    mask = *((long int *) &test);
    mask2 = (mask>>52) & 03777;
    mask3 = (mask<<(64-52))>>(64-52);

    temp = *((long int *) &copy);
    temp2 = (temp>>52) & 03777;
    temp3 = (temp<<(64-52))>>(64-52);


    printf("\nHexadecimal Complete:%li %li\n",mask, temp);
    printf("Hexadecimal Complete:%li\n",mask-temp);
    printf("realvalue: %li %li\n", mask2, temp2);
    printf("Hexadecimal Complete:%li\n",mask2-temp2);
    printf("realvalue: %li %li\n", mask3, temp3);
    printf("Hexadecimal Complete:%li\n",mask3-temp3);
}


long int long_min(long int x, long int y){
    return ((x<=y) ? x : y);
}

long int long_max(long int x, long int y){
    return ((x>=y) ? x : y);
}

//to use give pow_2 a long int, it will create a power of 2, accoridng to IEEE standards, ie 2^i, i[-1023,1024]
double pow_2(long int exp){
    double divisor;
    *((long int *) &divisor) =  exp<<52;
    return divisor;
}

/************************************************************************
Equation Solvers
************************************************************************/
// for solving linear equations
int lin_root(double a1, double a0, double * r){
    //special cases
    if(a1==0){
        if(a0!=0){
            return -1;
        }
        return 0;
    }

    //standard cases
    *r = -a0/a1;
    return 1;
}

// for solving linear equations (float version)
int lin_root_float(float a1, float a0, float * r){
    //special cases
    if(a1==0){
        if(a0!=0){
            return -1;
        }
        return 0;
    }

    //standard cases
    *r = -a0/a1;
    return 1;
}


//for solving quadratic equations
int quad_root_traditional(double a2, double a1, double a0, double * r1, double * r2){
    //variable declaration
    int i;
    double det;

    // the linear case:
    if (a2==0){
        i = lin_root(a1,a0,r1);
        return i-2;
    }

    // a0 = 0 case:
    if(a0==0){
        i = lin_root(a2,a1,r1); // as a2 is != 0, we have that the linear equation always has one root

        //returning the value and roots the right way!
        if(*r1>0){
            *r2 = 0;
            return 2;
        }
        if (*r1==0){
            *r2 = 0;
            return 1;
        }
        if (*r1<0){
            *r2 = *r1;
            *r1 = 0;
            return 2;
        }
    }

    //quadratic case:
    det = a1*a1-4*a2*a0;

        //checking for the complex case
    if (det<0){
        return 0;
    }
        //checking for det = 0 case
    if (det==0){
        *r1 = -a1*(0.5/a2);
        *r2 = *r1;
        return 1;
    }
        //two distinct real roots
    *r1 = (-a1+sqrt(det))*(0.5/a2);
    *r2 = (-a1-sqrt(det))*(0.5/a2);
    return 2;
}


//for solving quadratic equations for assessment (precise)
int quad_root_float(float a2, float a1, float a0, float * r1, float * r2){
    //variable declaration
    int i;
    float det;

    // the linear case:
    if (a2==0){
        i = lin_root_float(a1,a0,r1);
        return i-2;
    }

    // a0 = 0 case:
    if(a0==0){
        i = lin_root_float(a2,a1,r1); // as a2 is != 0, we have that the linear equation always has one root

        //returning the value and roots the right way!
        if(*r1>0){
            *r2 = 0;
            return 2;
        }
        if (*r1==0){
            *r2 = 0;
            return 1;
        }
        if (*r1<0){
            *r2 = *r1;
            *r1 = 0;
            return 2;
        }
    }

    //quadratic case:
    det = a1*a1-4*a2*a0;

        //checking for the complex case
    if (det<0){
        return 0;
    }
        //checking for det = 0 case
    if (det==0){
        *r1 = -a1*(0.5/a2);
        *r2 = *r1;
        return 1;
    }
    //two distinct real roots (floating point version)
    if (a1<0){
        *r1 = -a1+sqrt(det)*(0.5/a2);
        *r2 = a0/(a2*(*r1));
        return 2;
    }
    else{
        *r2 = -a1-sqrt(det)*(0.5/a2);
        *r1 = a0/(a2*(*r2));
        return 2;
    }

}

//for solving quadratic equations for assessment (precise)
int quad_root_old(double a2, double a1, double a0, double * r1, double * r2){
    //variable declaration
    int i;
    double det;

    // the linear case:
    if (a2==0){
        i = lin_root(a1,a0,r1);
        return i-2;
    }

    // a0 = 0 case:
    if(a0==0){
        i = lin_root(a2,a1,r1); // as a2 is != 0, we have that the linear equation always has one root

        //returning the value and roots the right way!
        if(*r1>0){
            *r2 = 0;
            return 2;
        }
        if (*r1==0){
            *r2 = 0;
            return 1;
        }
        if (*r1<0){
            *r2 = *r1;
            *r1 = 0;
            return 2;
        }
    }

    //quadratic case:
    det = a1*a1-4*a2*a0;

        //checking for the complex case
    if (det<0){
        return 0;
    }
        //checking for det = 0 case
    if (det==0){
        *r1 = -a1*(0.5/a2);
        *r2 = *r1;
        return 1;
    }
    //two distinct real roots (floating point version)
    if (a1<0){
        *r1 = (-a1+sqrt(det))*(0.5/a2);
        *r2 = a0/(a2*(*r1));
        return 2;
    }
    else{
        *r2 = (-a1-sqrt(det))*(0.5/a2);
        *r1 = a0/(a2*(*r2));
        return 2;
    }

}

/*
*
*
* QUAD_ROOT (ASSESSMENT)
*
*
*/



int quad_root(double a2, double a1, double a0, double * r1, double * r2){
    /*<ROZANOV>,<NIKOLAI>,<M3SC>*/
    /*****************************************************
    *variable declaration
    *****************************************************/
    int i;
    double det, pow_2=1.0;
    long int my_exponent, a2_mask, a1_mask, a0_mask, min_mask, max_mask;



    /****************************************************
    *the linear case:
    *****************************************************/

    if (a2==0){
        i = lin_root(a1,a0,r1);
        return i-2;
    }

    // a0 = 0 case:
    if(a0==0){
        i = lin_root(a2,a1,r1); // as a2 is != 0, we have that the linear equation always has one root

        //returning the value and roots the right way!
        if(*r1>0){
            *r2 = 0;
            return 2;
        }
        if (*r1==0){
            *r2 = 0;
            return 1;
        }
        if (*r1<0){
            *r2 = *r1;
            *r1 = 0;
            return 2;
        }
    }


    /****************************************************
    *the quadratic case:
    *****************************************************/
    if (a1==0){
        if(-a0/a2<0){
            return 0;
        }
        else{
            *r1 = sqrt(-a0/a2);//assumes input is sensible!
            *r2 = -(*r1);
            return 2;
        }
    }


    /***********************
    *Diskriminant
    ************************/

    //determining the exponent(and sign) of the coefficients

    a2_mask = ((*((long int *) &a2)) >> 52);//  //determines the exponent of a double
    a1_mask = ((*((long int *) &a1)) >> 52);//
    a0_mask = ((*((long int *) &a0)) >> 52);//

    /*
    *MASKS to make use of a2_mask, a1_mask, a0_mask
    *
    *04000 to determine sign bit
    *03777 get rid of the sign bit.
    *
    */

    //determining min/max of the exponent of the coefficients
    min_mask    = fmin((a2_mask & 03777),fmin(a1_mask & 03777,a0_mask & 03777)); //minimum exponent of a2,a1,a0
    max_mask    = fmax((a2_mask & 03777),fmax(a1_mask & 03777,a0_mask & 03777)); //maximum exponent of a2,a1,a0


    /*************
    *taking care of various computational cases to compute Diskriminant
    **************/
    if( max_mask>(1023+24) ){
        my_exponent = 2046-max_mask; // determining by how much to divide
        *((long int *) &pow_2) =  my_exponent<<52; //creating the divisor (such that double precision is preserved)

        //computing det, preventing overflow
        a2 = a2*pow_2;
        a1 = a1*pow_2;
        a0 = a0*pow_2;
        det = (a1*a1)-((double)4)*(a2*a0) ;

        if(det<0){ //checking for the complex case
            *r1 = -a1/(((double)2) * a2);
            *r2 = sqrt(-det)/(((double)2) * a2);
            return 0;
        }

        det = sqrt(det)/pow_2;
    }
    else if( min_mask<(1023-24) && min_mask>(513) ){
        //my_exponent = 622+(1023-min_mask); // determining by how much to mulitply
        my_exponent = 1023-((1023-24)-min_mask); //determining by how much to divide
        *((long int *) &pow_2) =  my_exponent<<52;//creating the divisor (such that double precision is preserved)

        //computing det, taking into account underflow..
        a2 = a2*pow_2;
        a1 = a1*pow_2;
        a0 = a0*pow_2;
        det = (a1*a1)-((double)4)*(a2*a0) ;

        if(det<0){ //checking for the complex case
            *r1 = -a1/(((double)2) * a2);
            *r2 = sqrt(-det)/(((double)2) * a2);
            return 0;
        }

        det = sqrt(det)/pow_2;
    }
    else{ //standard case
        det = a1*a1-((double)4)*a2*a0;

        if(det<0){ //checking for the complex case
            *r1 = -a1/(((double)2) * a2);
            *r2 = sqrt(-det)/(((double)2) * a2);
            return 0;
        }

        else det = sqrt(det);
    }





    /****************************
    *solving the actual equation
    *****************************/

        //checking for det = 0 case
    if (det==0){
        *r1 = -a1/(((double)2) * a2);
        *r2 = *r1;
        return 1;
    }
    //two distinct real roots (floating point version)
    if (a1<0){
        *r1 = (-a1 + det*pow_2) /  (((double)2) * a2) ;
        *r2 = (a0/a2)/(*r1);
        return 2;
    }
    else{
        *r2 = (-a1 - det*pow_2) / ( ((double)2) * a2);
        *r1 = (a0/a2)/(*r2);
        return 2;
    }

}




/************************************************************************
Main Function
************************************************************************/
int main(void){

    /*******************************************************************
    ********************************************************************
    SOURCE for Radius:
        Charon: https://en.wikipedia.org/wiki/Charon_(moon)
        Pluto: https://en.wikipedia.org/wiki/Pluto
    ********************************************************************
    *******************************************************************/

    int i=0,j,k, done=0x0, r3, counter1=0, counter2=0, counter3=0;    //doubles
    double a2_,a1_,a0_, r1_dbl, r2_dbl, r1, r2;


    /*********************************************************
    //Main Part of the Program
    *********************************************************/

    //printf("%16s\t%16s\t%16s\t%16s\t%16s\t%16s\n",array_fields[0],array_fields[1],array_fields[2],array_fields[3],array_fields[4],array_fields[5]);


    while (done==0){
        scanf("%lf %lf %lf %d %lf %lf %d", &a2_,&a1_,&a0_,&done,&r2,&r1,&r3);

        if (r3>0){
            //printf("\n\n\nCASE:%d    %10.5e %10.5e %10.5e %10.5e %10.5e %d\n",i, a2_,a1_,a0_,r1,r2,r3);
            k = quad_root(a2_,a1_,a0_,&r1_dbl,&r2_dbl);
            //printf("ROOT1:%3.16lf   %3.16lf return: %d his return: %d\n",r1_dbl-r1,r2_dbl-r2,k, r3);

            if (r1_dbl-r1>0.01){
                counter1++;
            }

            if (r2_dbl-r2>0.01){
                counter2++;
            }
            if (k!=r3){
                counter3++;
            }

            i++;
        }


    }

    printf("\nCases failed: r1: %d  r2: %d  return: %d out of: %d  precentage of passing cases: %f \n", counter1, counter2, i-counter3,i,((float)(counter1+counter2))/((float)i));


    return 0;
}




//
